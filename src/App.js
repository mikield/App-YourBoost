import Vue from 'vue'
import Pace from 'pace-progress'
var pjson = require('../package.json');

class Application{
  constructor(router, store, tranlator){
    this.RequireStyles()
    this.router = router;
    this.store = store;
    this.tranlator = tranlator;
    Vue.prototype.$events = new Vue()
  }

  get data(){
    return {
      APP_VERSION: pjson.version,
      fallbackLang: 'en-US',
      langs: {
        'ru': {
          name: 'Русский',
          icon: require('assets/images/langs/ru.png'),
          code: 'ru'
        },
        'en-US': {
          name: 'English',
          icon: require('assets/images/langs/en.svg'),
          code: 'en-US'
        },
      }
    }
  }
  get name(){
    return 'Application'
  }

  get mounted(){
    return function(){
      Pace.start()
      if(localStorage.getItem('lang') === null || this.langs[this.$i18n.locale] === undefined){
        this.ChangeLocale(this.fallbackLang)
      }    
    }
  }

  get methods(){
    return {
      ChangeLocale(code){
        this.$i18n.locale = code
        localStorage.setItem('lang', `${code}`)
        this.$events.$emit('Language/Changed', code)
      }
    }
  }

  RequireStyles(){
    require('bootstrap/dist/css/bootstrap.css')
    require('font-awesome/css/font-awesome.css')
    require('ionicons/css/ionicons.css')
    require('./assets/styles/app.min.css')
    require('./assets/styles/font.css')
    require('./assets/theme/accent.css')
  }

  run(){
    new Vue({
      name: this.name,
      el: '#app',
      data: this.data,
      render: h => h("div", {}, ['vue-progress-bar','router-view'].map((n) => {
          return h(n)
      })),
      router: this.router,
      store: this.store,
      i18n: this.tranlator,
      mounted: this.mounted,
      methods: this.methods
    })
  }
}

module.exports = Application
