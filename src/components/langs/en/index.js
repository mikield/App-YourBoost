const FAQ = require('./FAQ')
module.exports = {
  FAQ,
  Welcome: {
    Login: 'Login',
    Intro: 'A service that gives you the option to boost your streams',
    Slogan: 'Bang, Bang, TOP',
    MoreInfo: 'Learn more',
    GoToPanel: 'Go to dashboard',
    YourBoost: 'YourBoost - Your speed up',
    YourBoostDescription:
      'YourBoost offers a tool to attract a new audience to your broadcast. Looking through the broadcasts on our service - you can earn an internal currency (energy) that you can spend on your promotion, or exchange for real money. With the help of our service, you can solve many problems such as:',
    YourBoostList: {
      first: 'Low view count on your streams',
      second: 'Low follower count',
      third: 'No popularity',
      for: 'Not enough earnings'
    },
    Features: {
      title: 'Features',
      '1': {
        title: '100% live people',
        description:
          'Only people who want to watch your stream will go to your stream. We do not and never will have bots or proxy viewers.'
      },
      '2': {
        title: 'Knowledge Base',
        description:
          'We try to collect all possible information about streams and how to improve them.'
      },
      '3': {
        title: 'Statistics',
        description:
          'Using our service you can get access to the statistics of your channels. Evaluating those statistics can make your streams more efficient.'
      },
      '4': {
        title: 'Time Control',
        description:
          'Set a limited viewing time, and monitor your costs. You can also set the stop time for promotion.'
      },
      '5': {
        title: 'Configurability',
        description:
          'When setting up your promotions you will have access to a huge selection of configurations.'
      },
      '6': {
        title: 'Additional tools',
        description:
          'In this service there are many more tools for your streams, such as bots, overlays, and more.'
      }
    },
    Services: {
      title: 'All the platoforms we work with',
      description:
        'We try to cover all the popular streaming sites. Some of them already work wth us, and others will be in the near future.'
    }
  },
  Layouts: {
    Front: {
      Outro:
        "Are you interested? Then let's proceed. After authorization you willl be met by our assistant.",
      Start: 'Start',
      Footer: {
        title: 'YourBoost is',
        description: 'A modern and convenient tool for your stream promotion..',
        additional:
          'You can go from a personal channel to a large scale projects in just a couple clicks.',
        MadeBy:
          "Made with <i class='fa fa-heart text-danger'></i> by <a target='_blank' class='text-success' href='//www.twitch.tv/admiralmiki'>AdmiralMiki</a> supported by <a target='_blank' class='text-warn' href='//streamersconnected.tv/'>SC Team</a>"
      },
      Header: {
        Opportunities: 'Opportunities',
        Services: 'Services',
        About: 'About',
        Logout: 'Logout',
        Login: 'Login'
      }
    },
    App: {
      Menu: {
        Broadcasts: 'Broadcasts',
        Hosting: 'Hosting',
        Promo: 'Promos',
        TakeBonus: 'Take',
        Settings: 'Settings',
        Help: 'Need help?',
        Logout: 'Logout'
      },
      TopMenu: {
        Challenges: 'Challenges',
        Help: 'Help'
      },
      Channels: {
        AddChannel: 'Add channel',
        Add: 'Add',
        Choose: 'Choose',
        RemoveChannel: 'Remove channel'
      },
      Footer: {
        MadeBy:
          "Made with <i class='fa fa-heart text-danger'></i> by <a target='_blank' class='text-success' href='//www.twitch.tv/admiralmiki'>AdmiralMiki</a> supported by <a target='_blank' class='text-warn' href='//streamersconnected.tv/'>SC Team</a>",
        Version: 'Version'
      }
    }
  },
  Login: {
    title: 'First time here??',
    description:
      "No worries, if it's your first time we will give you some help.",
    LoginWithTwitch: 'Login with Twitch',
    LoginWithTwitter: 'Login with Twitter',
    LoginWithMixer: 'Login with Mixer',
    LoginWithVk: 'Login with Vk',
    Alerts: {
      Pending: 'Logging in...',
      Success: 'Welcome, {username}',
      Error: 'Sorry, login error. Try again.'
    }
  },
  Dashboard: {
    title: 'Promoted streams',
    description: 'List of promoted stream',
    Update: 'Raise your stream?',
    Stop: 'Stop the promotion?',
    Add: 'Add your stream?'
  },
  View: {
    Counters: {
      NowLooking: 'Live viewers',
      Views: 'Total views',
      Followers: 'Total Followers'
    },
    View: 'Broadcast',
    Viewers: 'Viewers',
    You: 'you',
    Minute: 'min.'
  },
  Hosts: {
    PageTitle: 'List of hosters',
    Title: 'List of hosters',
    Description: 'Hosters who are ready to host you',
    AddButton: 'Add your channel',
    RemoveButton: 'Remove your channel from the list?',
    GetPrize: 'Check cost',
    YourChannel: 'Your channel',
    Views: 'Views',
    Followers: 'Followers'
  },
  Settings: {
    General: {
      Menu: 'General',
      Title: 'Profile Settings',
      ChannelNotChoosen: 'First you need to select a channel',
      Input1: 'About',
      Button: 'Update {for}',
      For: ' for {channel}'
    },
    Social: {
      Menu: 'Social',
      Title: 'Social Profiles',
      Facebook: 'Login with Facebook',
      Vk: 'Login with VK',
      Twitter: 'Login with Twitter',
      Youtube: 'Login with YouTube',
      Button: 'Update'
    }
  },
  Events: {
    Title: 'Calender of reservations',
    Description:
      'On this calendar, you will be able to schedule promotion in the promo-block on a specific day for a specific time. Reservation occurs on the exact time, which means that in the time of the broadcast, the broadcast must go online from the channel you have just selected, otherwise the broadcast will not be shown (without refund)',
    Reserve: 'Reserve'
  },
  Modals: {
    Cancel: 'Cancel',
    Events: {
      New: {
        Title: 'New reservation in promo-block',
        Description:
          'Select the game that wou will stream. Viewers will know what game to expect on your stream.',
        Cost: 'Reservation will cost:',
        Game: 'Game',
        Label: 'Ignore',
        Submit: 'Reserve'
      },
      Remove: {
        Title: 'Are you sure you want to cancel your reservation?',
        Description:
          'Canceling the reservation, we will refund only 50% of the reservation price',
        Cost: 'After confirming on your balance, take:',
        Submit: 'Confirm'
      }
    },
    Hosting: {
      AddToList: {
        Title: 'Add Channel',
        SubTitle: 'Add channel to host list',
        Description:
          "Your current channel will be added to the hosters list. Based on the details of your channel, the person who will be hosted by you will bring you: <b class='text-success'>{earn} <i class='fa fa-bolt'></i></b>/min.<br> Adding a channel will cost you: <b class='text-danger'>{lost} <i class='fa fa-bolt'></i>.</b><br>",
        Add: 'Add'
      },
      GetPrize: {
        Title: 'Get in touch',
        Description: 'Get in touch with <b>{channel}</b>',
        Body:
          "The selected user will host you. <br>  Based on the details of the hoster's channel, you will spend: <b class='text-danger'>{cost} <i class='fa fa-bolt'></i>/min.</b>",
        Submit: 'Get in touch'
      }
    },
    Streams: {
      Add: {
        Title: 'Add Stream',
        SubTitle: 'Add your stream to the promotion list',
        Description:
          "You can leave everything a sis, and just click 'Add', or change the options below.<br>Attention: After the operation, there will be an additional payment, but it will significantly improve the quality of your promotion.<br>Initial promotion price: <bclass='text-danger'>1000<iclass='fafa-bolt'></i></b>+<bclass='text-danger'>150<iclass='fafa-bolt'></i>/min.</b>",
        Options: {
          '1': {
            Title:
              "<input type='checkbox' name='muted'><i class='dark-white'></i>Disallow user to mute your Broadcast <b class='text-danger'>+150 <i class='fa fa-bolt'></i>/min.</b>",
            Description:
              'Users who are viewing your broadcast, can not mute it completely.'
          },
          '2': {
            Title:
              "Set the limit  <b><i class='fa fa-bolt'></i></b> for your stream <small class='text-success'>(optional)</small></b>"
          }
        },
        Add: 'Add'
      }
    },
    Channels: {
      Add: {
        Title: 'Add channel',
        SubTitle: 'Add your channel to the list.',
        Description:
          'Select the platform whose channel you want to add. After clicking on the corresponding icon, the authorization window will open.'
      },
      Found: {
        Title: 'We found the channel',
        Description:
          'We detected your channel <b>{channel}</b>. You can add it right away or later.',
        Add: 'Add'
      }
    },
    View: {
      Muted: {
        Title: 'Stream is muted',
        Description:
          'You muted the stream, although the streamer forbade it! We have suspended your earnings.',
        Submit: 'Enable sound (25%)',
        Cancel: 'Ignore'
      },
      NoMoneyLeft: {
        Title: "Ended <i class='fa fa-bolt'></i>",
        Description:
          "The host ended the stream <i class='fa fa-bolt'></i> and they will not be able to pay you the next minute of viewing. We suspended your earnings.",
        Submit: 'Leave page',
        Cancel: 'Stay on page'
      },
      Offline: {
        Title: 'Stream ended',
        Description:
          '<p>The streamer has finished the broadcast, and the earnings have automatically stopped.</p><p>You can leave the page, or stay on it.</p>',
        Submit: 'Leave page',
        Cancel: 'Stay on page'
      },
      Paused: {
        Title: 'Stream paused',
        Description:
          'You put the broadcast on a pause! As a result we have suspended your earnings.',
        Submit: 'Play',
        Cancel: 'Ignore'
      }
    }
  },
  Alerts: {
    Headers: {
      Success: {
        '0': 'Excellent!',
        '1': 'Hooray!',
        '2': 'Yuppieeee!',
        '3': 'Everything cool!',
        '4': 'Boom!'
      },
      Error: {
        '0': 'Wow!',
        '1': 'Owe you',
        '2': 'Sorry, but...',
        '3': 'Trouble',
        '4': 'Boom!'
      },
      Info: {
        '0': "It's amazing, but...",
        '1': 'Wait a second...'
      }
    },
    BonusSuccess: 'Recieve your daily bonus!',
    Stream: {
      NotOnline:
        "Stream currently <b class='text-danger'>offline</b>. It is necessary that the stream is <b class='text-success'>online</b>",
      Online:
        "Stream currently <b class='text-success'>online</b>. It is necessary that the stream is <b class='text-danger'>offline</b>.",
      InPromotedList: 'The stream is already on the promotion list',
      Removed:
        'Your broadcast has been successfully removed from promotion list',
      Added: 'Your broadcast has been successfully added to promotion list'
    },
    Dashboard: {
      StreamIsFirst: 'Your broadcast is first. No need to boost it '
    },
    User: {
      LowBalance:
        "You do not have enough <i class='fa fa-bolt'></i> for this option.",
      NoChannel:
        'You did not select a channel. You can select it in the upper right corner.',
      Offline: 'Your stream is now offline.'
    },
    Channel: {
      DoesNotExists: 'The selected channel does not exist.',
      ChoseeInfo: 'You can choose a channel in the upper right corner.'
    },
    Hoster: {
      Online: 'Hoster is online',
      NotInList: 'Something strange happened',
      NotFound: 'Something strange happened',
      IsHosting: 'Hoster is already hosting.'
    },
    View: {
      SelfLooking: 'Looking at your own broadcast, will not earn you anything.'
    },
    Success: {
      Hosting_AddedToList: 'Your channel has been added to the hosting list',
      Events_RemovedFromList: 'Reservation successfully canceled',
      Events_AddedToList: 'Reservation successfully used',
      Hosting_StartedHosting: 'Hooray! Successfully began to host {channel}',
      Hosting_RemovedFromList:
        'Your channel has been removed from the host list.',
      Channel_Added:
        'The channel has been successfully linked to your account!',
      Settings_Updated: 'We have saved your settings.'
    }
  }
}
