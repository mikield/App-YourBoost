module.exports = {
  Title: 'Welcome to BoostMy.Stream Frequently Asked Questions',
  search: 'Search...',
  shorts: {
    q: 'Q',
    a: 'A'
  },
  Questions: [
    {
      category: 'General',
      q: 'What is BoostMy.Stream?',
      a:
        'BoostMy.Stream is a promotion platform for beginners and experienced streamers who want to attract more audience to their broadcasts. \
  We offer the best tools for promoting, collecting statistics and improving your broadcast. For this we use advanced technologies that allow us to provide the best service.'
    },
    {
      category: 'General',
      q: 'How BoostMy.Stream works?',
      a:
        'At BoostMy.Stream you can promote your online broadcast. When you add your broadcast to the promotion - you will pay each of your online viewers by the formula: number of viewers * price_to_minute. \
Why do you need this? To be a popular streamer, you need to be noticed. To be noticed in most cases you need charisma, as well as be in the top on your streaming platform. Problems with charisma, we are not able to solve, but here in the top we will definitely help you get out. But many popular tape drives use bots (not live people) to simulate online, we do not do that. For you to broadcast live 100% live people who at the moment will watch you <small> (but we will not tell what they paid for it) </small>. These people will raise you only to the top, and the rest is yours;)'
    },
    {
      category: 'Authorization',
      q: 'How to register/login into the system?',
      a:
        'We have long thought how to simplify the registration / authorization system, and decided - one button for everything. If you have not already been in our system - we will create a new account for you, and we will take the data for it from the public data of the social network through which you will enter.'
    },

    {
      category: 'Account',
      q: 'What is an account and why it`s needed?',
      a:
        'We strive to work with all popular services, and each service has Accounts, in some of them there are many channels, so that we could not omit this moment and also divided the system into Accounts and Channels. So to put it briefly, the account is a record of your account on the streaming site.'
    },
    {
      category: 'Account',
      q: 'How to add an account?',
      a:
        'To add an account - you need to go to the Accounts <small>(you can find the menu after clicking on your profile picture in the left bottom corner)</small> page and select the service whose account you want to add in the upper right corner. After that, a window will open in which you will have to confirm access to your account information, and wait until we write about it in the system.'
    },

    {
      category: 'Channel',
      q: 'What is an account and why it`s needed?',
      a:
        'Channels are the same as the channels on your streaming platforms, you can add multiple channels and switch in front of them to work with them.'
    },
    {
      category: 'Channel',
      q: 'How to add a channel?',
      a:
        'To add a channel you need to click on the + icon in the upper right corner and click on the "Add Channel" button, and then in the modal window select the account whose channel you want to add.'
    },
    {
      category: 'Channel',
      q: 'How to choose a channel?',
      a:
        'To select a channel you need to click on the "+" in the upper right corner and click on the green button, then the "+" icon should change to the avatar of your channel.'
    },

    {
      category: 'Promotion',
      q: 'How to start the promotion?',
      a:
        'In order for your promotion to start, you must add your broadcast to the list of promoted broadcasts. To do this, go to the page of promoted broadcasts, and then in the right corner near the "Add your broadcast?" click on the plus sign. In the modal window, you will be offered several options for configuring your promotion, by default we have set the most optimal settings. Click "Add" when finished with the settings.'
    },
    {
      category: 'Promotion',
      q: 'How to earn on the promotions?',
      a:
        'To earn on promotion, you need to look at someone else`s progress. Go to the promotion page and choose the best broadcast <small> (the first 3 in the list will give you more coins for each minute of viewing) </small>. After the bottom of the left corner of the band begin to fill, you can easily enjoy the broadcast, and the band will be zeroed out every minute and bring you coins.'
    }
  ]
}
