module.exports = {
  "name": "FAQ",
  "icon": '<i class="fa fa-question"></i>',
  "Title": "FAQ",
  "Body": '<div class="m-b" id="accordion">\
    <div class="panel box no-border m-b-xs">\
      <div class="box-header p-y-sm">\
        <a data-toggle="collapse" data-parent="#accordion" data-target="#c_1">Q: Есть ли возможность контакта с создателями?</a>\
      </div>\
      <div id="c_1" class="collapse">\
        <div class="box-body">\
          <div class="text-sm text-muted">\
            <div class="pull-left m-r"><span class="text-md w-32 avatar rounded success">A</span></div>\
            <p>Да конечно же есть, вы можете использовать виджет Вконтакте внизу страницы.</p>\
          </div>\
        </div>\
      </div>\
    </div>\
    <div class="panel box no-border m-b-xs">\
    <div class="box-header p-y-sm">\
      <a data-toggle="collapse" data-parent="#accordion" data-target="#c_2">Q: Когда проект перестанет быть альфой?</a>\
    </div>\
    <div id="c_2" class="collapse">\
      <div class="box-body">\
        <div class="text-sm text-muted">\
          <div class="pull-left m-r"><span class="text-md w-32 avatar rounded success">A</span></div>\
          <p>У нас много идей и планов на проект, но когда он сможет показать себя миру мы точно не знаем. Мы стараемся сделать все возможное чтобы скоротить этот срок.</p>\
        </div>\
      </div>\
    </div>\
  </div>\
'
}
