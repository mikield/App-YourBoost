module.exports = {
  "name": "О проекте",
  "icon": "<i class='ion-android-boat'></i>",
  "Title": "Yourboost - платформа продвижения",
  "Body": 'Одним из главных преимуществ YourBoost является продвижение ваших стримов легким и бесплатным! Как это работает:\
  <div class="streamline">\
   <div class="sl-item b-warning">\
      <div class="sl-content">\
         <div class="sl-date text-muted">Сначала</div>\
         <div>Вы добавляете свой стрим в пиар</div>\
      </div>\
   </div>\
   <div class="sl-item b-info">\
      <div class="sl-content">\
         <div class="sl-date text-muted">После</div>\
         <div>На вашу трансляцию начинают приходить люди</div>\
      </div>\
   </div>\
   <div class="sl-item b-success">\
   <div class="sl-icon">\
      <i class="fa fa-check"></i>\
   </div>\
      <div class="sl-content">\
         <div class="sl-date text-muted">И наконец</div>\
         <div>\
            Другие пользователи системы смотрят ваш стрим, получая за это внутреннюю валюту и тем самым поднимая популярность стрима.\
         </div>\
      </div>\
   </div>\
</div><br>\
Как же заработать <i class="fa fa-bolt"></i>? Так же легко как начать свое продвижение:\
<div class="streamline">\
 <div class="sl-item b-warning">\
    <div class="sl-content">\
       <div class="sl-date text-muted">Сначала</div>\
       <div>Заходим на продвигаемую трансляцию</div>\
    </div>\
 </div>\
 <div class="sl-item b-info">\
    <div class="sl-content">\
       <div class="sl-date text-muted">После</div>\
       <div>Ждем пока откроеться плеер</div>\
    </div>\
 </div>\
 <div class="sl-item b-success">\
 <div class="sl-icon">\
    <i class="fa fa-check"></i>\
 </div>\
    <div class="sl-content">\
       <div class="sl-date text-muted">И наконец</div>\
       <div>\
        Просматриваем трансляцию пользователя, тем самым получая каждый раз <i class="fa fa-bolt"></i> на свой счет.\
       </div>\
    </div>\
 </div>\
</div>'
}
