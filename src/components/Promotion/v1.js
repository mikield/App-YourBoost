! function(t, e) {
    "object" == typeof exports && "object" == typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define([], e) : "object" == typeof exports ? exports.video = e() : (t.Twitch = t.Twitch || {}, t.Twitch.video = e())
}(this, function() {
    return function(t) {
        function e(r) {
            if (n[r]) return n[r].exports;
            var o = n[r] = {
                i: r,
                l: !1,
                exports: {}
            };
            return t[r].call(o.exports, o, o.exports, e), o.l = !0, o.exports
        }
        var n = {};
        return e.m = t, e.c = n, e.i = function(t) {
            return t
        }, e.d = function(t, n, r) {
            e.o(t, n) || Object.defineProperty(t, n, {
                configurable: !1,
                enumerable: !0,
                get: r
            })
        }, e.n = function(t) {
            var n = t && t.__esModule ? function() {
                return t.default
            } : function() {
                return t
            };
            return e.d(n, "a", n), n
        }, e.o = function(t, e) {
            return Object.prototype.hasOwnProperty.call(t, e)
        }, e.p = "", e(e.s = 157)
    }([function(t, e, n) {
        var r = n(32),
            o = "object" == typeof self && self && self.Object === Object && self,
            i = r || o || Function("return this")();
        t.exports = i
    }, function(t, e) {
        var n = Array.isArray;
        t.exports = n
    }, function(t, e, n) {
        function r(t) {
            return null == t ? void 0 === t ? c : a : s && s in Object(t) ? i(t) : u(t)
        }
        var o = n(7),
            i = n(99),
            u = n(130),
            a = "[object Null]",
            c = "[object Undefined]",
            s = o ? o.toStringTag : void 0;
        t.exports = r
    }, function(t, e, n) {
        function r(t, e) {
            var n = i(t, e);
            return o(n) ? n : void 0
        }
        var o = n(73),
            i = n(101);
        t.exports = r
    }, function(t, e, n) {
        function r(t, e, n, r) {
            var u = !n;
            n || (n = {});
            for (var a = -1, c = e.length; ++a < c;) {
                var s = e[a],
                    f = r ? r(n[s], t[s], s, n, t) : void 0;
                void 0 === f && (f = t[s]), u ? i(n, s, f) : o(n, s, f)
            }
            return n
        }
        var o = n(17),
            i = n(29);
        t.exports = r
    }, function(t, e) {
        function n(t) {
            var e = typeof t;
            return null != t && ("object" == e || "function" == e)
        }
        t.exports = n
    }, function(t, e) {
        function n(t) {
            return null != t && "object" == typeof t
        }
        t.exports = n
    }, function(t, e, n) {
        var r = n(0),
            o = r.Symbol;
        t.exports = o
    }, function(t, e, n) {
        function r(t) {
            var e = -1,
                n = null == t ? 0 : t.length;
            for (this.clear(); ++e < n;) {
                var r = t[e];
                this.set(r[0], r[1])
            }
        }
        var o = n(115),
            i = n(116),
            u = n(117),
            a = n(118),
            c = n(119);
        r.prototype.clear = o, r.prototype.delete = i, r.prototype.get = u, r.prototype.has = a, r.prototype.set = c, t.exports = r
    }, function(t, e, n) {
        function r(t, e) {
            for (var n = t.length; n--;)
                if (o(t[n][0], e)) return n;
            return -1
        }
        var o = n(22);
        t.exports = r
    }, function(t, e, n) {
        function r(t, e) {
            var n = t.__data__;
            return o(e) ? n["string" == typeof e ? "string" : "hash"] : n.map
        }
        var o = n(113);
        t.exports = r
    }, function(t, e) {
        function n(t) {
            var e = t && t.constructor;
            return t === ("function" == typeof e && e.prototype || r)
        }
        var r = Object.prototype;
        t.exports = n
    }, function(t, e, n) {
        var r = n(3),
            o = r(Object, "create");
        t.exports = o
    }, function(t, e, n) {
        function r(t) {
            return null != t && i(t.length) && !o(t)
        }
        var o = n(44),
            i = n(45);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return u(t) ? o(t) : i(t)
        }
        var o = n(26),
            i = n(75),
            u = n(13);
        t.exports = r
    }, function(t, e, n) {
        var r = n(3),
            o = n(0),
            i = r(o, "Map");
        t.exports = i
    }, function(t, e) {
        function n(t, e) {
            for (var n = -1, r = e.length, o = t.length; ++n < r;) t[o + n] = e[n];
            return t
        }
        t.exports = n
    }, function(t, e, n) {
        function r(t, e, n) {
            var r = t[e];
            a.call(t, e) && i(r, n) && (void 0 !== n || e in t) || o(t, e, n)
        }
        var o = n(29),
            i = n(22),
            u = Object.prototype,
            a = u.hasOwnProperty;
        t.exports = r
    }, function(t, e, n) {
        function r(t, e) {
            return o(t) ? t : i(t, e) ? [t] : u(a(t))
        }
        var o = n(1),
            i = n(112),
            u = n(139),
            a = n(150);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            var e = new t.constructor(t.byteLength);
            return new o(e).set(new o(t)), e
        }
        var o = n(59);
        t.exports = r
    }, function(t, e, n) {
        var r = n(36),
            o = r(Object.getPrototypeOf, Object);
        t.exports = o
    }, function(t, e, n) {
        var r = n(65),
            o = n(47),
            i = Object.prototype,
            u = i.propertyIsEnumerable,
            a = Object.getOwnPropertySymbols,
            c = a ? function(t) {
                return null == t ? [] : (t = Object(t), r(a(t), function(e) {
                    return u.call(t, e)
                }))
            } : o;
        t.exports = c
    }, function(t, e) {
        function n(t, e) {
            return t === e || t !== t && e !== e
        }
        t.exports = n
    }, function(t, e, n) {
        function r(t) {
            return "symbol" == typeof t || i(t) && o(t) == u
        }
        var o = n(2),
            i = n(6),
            u = "[object Symbol]";
        t.exports = r
    }, function(t, e) {
        t.exports = function(t) {
            return t.webpackPolyfill || (t.deprecate = function() {}, t.paths = [], t.children || (t.children = []), Object.defineProperty(t, "loaded", {
                enumerable: !0,
                get: function() {
                    return t.l
                }
            }), Object.defineProperty(t, "id", {
                enumerable: !0,
                get: function() {
                    return t.i
                }
            }), t.webpackPolyfill = 1), t
        }
    }, function(t, e, n) {
        function r(t) {
            var e = -1,
                n = null == t ? 0 : t.length;
            for (this.clear(); ++e < n;) {
                var r = t[e];
                this.set(r[0], r[1])
            }
        }
        var o = n(120),
            i = n(121),
            u = n(122),
            a = n(123),
            c = n(124);
        r.prototype.clear = o, r.prototype.delete = i, r.prototype.get = u, r.prototype.has = a, r.prototype.set = c, t.exports = r
    }, function(t, e, n) {
        function r(t, e) {
            var n = u(t),
                r = !n && i(t),
                f = !n && !r && a(t),
                p = !n && !r && !f && s(t),
                h = n || r || f || p,
                v = h ? o(t.length, String) : [],
                d = v.length;
            for (var y in t) !e && !l.call(t, y) || h && ("length" == y || f && ("offset" == y || "parent" == y) || p && ("buffer" == y || "byteLength" == y || "byteOffset" == y) || c(y, d)) || v.push(y);
            return v
        }
        var o = n(80),
            i = n(42),
            u = n(1),
            a = n(43),
            c = n(35),
            s = n(145),
            f = Object.prototype,
            l = f.hasOwnProperty;
        t.exports = r
    }, function(t, e) {
        function n(t, e) {
            for (var n = -1, r = null == t ? 0 : t.length, o = Array(r); ++n < r;) o[n] = e(t[n], n, t);
            return o
        }
        t.exports = n
    }, function(t, e) {
        function n(t, e, n, r) {
            var o = -1,
                i = null == t ? 0 : t.length;
            for (r && i && (n = t[++o]); ++o < i;) n = e(n, t[o], o, t);
            return n
        }
        t.exports = n
    }, function(t, e, n) {
        function r(t, e, n) {
            "__proto__" == e && o ? o(t, e, {
                configurable: !0,
                enumerable: !0,
                value: n,
                writable: !0
            }) : t[e] = n
        }
        var o = n(31);
        t.exports = r
    }, function(t, e, n) {
        function r(t, e, n) {
            var r = e(t);
            return i(t) ? r : o(r, n(t))
        }
        var o = n(16),
            i = n(1);
        t.exports = r
    }, function(t, e, n) {
        var r = n(3),
            o = function() {
                try {
                    var t = r(Object, "defineProperty");
                    return t({}, "", {}), t
                } catch (t) {}
            }();
        t.exports = o
    }, function(t, e, n) {
        (function(e) {
            var n = "object" == typeof e && e && e.Object === Object && e;
            t.exports = n
        }).call(e, n(48))
    }, function(t, e, n) {
        function r(t) {
            return o(t, u, i)
        }
        var o = n(30),
            i = n(34),
            u = n(46);
        t.exports = r
    }, function(t, e, n) {
        var r = n(16),
            o = n(20),
            i = n(21),
            u = n(47),
            a = Object.getOwnPropertySymbols,
            c = a ? function(t) {
                for (var e = []; t;) r(e, i(t)), t = o(t);
                return e
            } : u;
        t.exports = c
    }, function(t, e) {
        function n(t, e) {
            return !!(e = null == e ? r : e) && ("number" == typeof t || o.test(t)) && t > -1 && t % 1 == 0 && t < e
        }
        var r = 9007199254740991,
            o = /^(?:0|[1-9]\d*)$/;
        t.exports = n
    }, function(t, e) {
        function n(t, e) {
            return function(n) {
                return t(e(n))
            }
        }
        t.exports = n
    }, function(t, e, n) {
        function r(t, e, n) {
            return e = i(void 0 === e ? t.length - 1 : e, 0),
                function() {
                    for (var r = arguments, u = -1, a = i(r.length - e, 0), c = Array(a); ++u < a;) c[u] = r[e + u];
                    u = -1;
                    for (var s = Array(e + 1); ++u < e;) s[u] = r[u];
                    return s[e] = n(c), o(t, this, s)
                }
        }
        var o = n(63),
            i = Math.max;
        t.exports = r
    }, function(t, e, n) {
        var r = n(78),
            o = n(133),
            i = o(r);
        t.exports = i
    }, function(t, e, n) {
        function r(t) {
            if ("string" == typeof t || o(t)) return t;
            var e = t + "";
            return "0" == e && 1 / t == -i ? "-0" : e
        }
        var o = n(23),
            i = 1 / 0;
        t.exports = r
    }, function(t, e) {
        function n(t) {
            if (null != t) {
                try {
                    return o.call(t)
                } catch (t) {}
                try {
                    return t + ""
                } catch (t) {}
            }
            return ""
        }
        var r = Function.prototype,
            o = r.toString;
        t.exports = n
    }, function(t, e) {
        function n(t) {
            return t
        }
        t.exports = n
    }, function(t, e, n) {
        var r = n(72),
            o = n(6),
            i = Object.prototype,
            u = i.hasOwnProperty,
            a = i.propertyIsEnumerable,
            c = r(function() {
                return arguments
            }()) ? r : function(t) {
                return o(t) && u.call(t, "callee") && !a.call(t, "callee")
            };
        t.exports = c
    }, function(t, e, n) {
        (function(t) {
            var r = n(0),
                o = n(149),
                i = "object" == typeof e && e && !e.nodeType && e,
                u = i && "object" == typeof t && t && !t.nodeType && t,
                a = u && u.exports === i,
                c = a ? r.Buffer : void 0,
                s = c ? c.isBuffer : void 0,
                f = s || o;
            t.exports = f
        }).call(e, n(24)(t))
    }, function(t, e, n) {
        function r(t) {
            if (!i(t)) return !1;
            var e = o(t);
            return e == a || e == c || e == u || e == s
        }
        var o = n(2),
            i = n(5),
            u = "[object AsyncFunction]",
            a = "[object Function]",
            c = "[object GeneratorFunction]",
            s = "[object Proxy]";
        t.exports = r
    }, function(t, e) {
        function n(t) {
            return "number" == typeof t && t > -1 && t % 1 == 0 && t <= r
        }
        var r = 9007199254740991;
        t.exports = n
    }, function(t, e, n) {
        function r(t) {
            return u(t) ? o(t, !0) : i(t)
        }
        var o = n(26),
            i = n(76),
            u = n(13);
        t.exports = r
    }, function(t, e) {
        function n() {
            return []
        }
        t.exports = n
    }, function(t, e) {
        var n;
        n = function() {
            return this
        }();
        try {
            n = n || Function("return this")() || (0, eval)("this")
        } catch (t) {
            "object" == typeof window && (n = window)
        }
        t.exports = n
    }, function(t, e, n) {
        "use strict";

        function r(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
        }
        n.d(e, "A", function() {
            return _
        }), n.d(e, "B", function() {
            return m
        }), n.d(e, "C", function() {
            return w
        }), n.d(e, "D", function() {
            return x
        }), n.d(e, "E", function() {
            return j
        }), n.d(e, "F", function() {
            return O
        }), n.d(e, "e", function() {
            return E
        }), n.d(e, "a", function() {
            return A
        }), n.d(e, "b", function() {
            return S
        }), n.d(e, "c", function() {
            return k
        }), n.d(e, "d", function() {
            return P
        }), n.d(e, "G", function() {
            return T
        }), n.d(e, "g", function() {
            return M
        }), n.d(e, "h", function() {
            return L
        }), n.d(e, "n", function() {
            return R
        }), n.d(e, "p", function() {
            return I
        }), n.d(e, "o", function() {
            return C
        }), n.d(e, "i", function() {
            return D
        }), n.d(e, "s", function() {
            return z
        }), n.d(e, "m", function() {
            return F
        }), n.d(e, "j", function() {
            return N
        }), n.d(e, "k", function() {
            return U
        }), n.d(e, "l", function() {
            return V
        }), n.d(e, "t", function() {
            return B
        }), n.d(e, "q", function() {
            return W
        }), n.d(e, "r", function() {
            return q
        }), n.d(e, "u", function() {
            return $
        }), n.d(e, "v", function() {
            return H
        }), n.d(e, "w", function() {
            return Y
        }), n.d(e, "x", function() {
            return K
        }), n.d(e, "z", function() {
            return Q
        }), n.d(e, "y", function() {
            return J
        }), n.d(e, "f", function() {
            return X
        });
        var o = n(143),
            i = n.n(o),
            u = n(148),
            a = n.n(u),
            c = n(140),
            s = n.n(c),
            f = n(152),
            l = n.n(f),
            p = n(51),
            h = n(52),
            v = n(53),
            d = (n.n(v), function() {
                function t(t, e) {
                    for (var n = 0; n < e.length; n++) {
                        var r = e[n];
                        r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(t, r.key, r)
                    }
                }
                return function(e, n, r) {
                    return n && t(e.prototype, n), r && t(e, r), e
                }
            }()),
            y = function() {
                var t = "https://player.twitch.tv";
                /** SOURCE MODIFICATION
                 *  We are loading this from within a different script file.
                 *  In order to get the request origins correct to load the videos, it has to be hard coded.
                 **/ 
                //if (document.currentScript)
                //	t = document.currentScript.src;
                //else {
                //	var e = Array.prototype.filter.call(document.scripts, function (t) {
                //			return /twitch\.tv.*embed/.test(t.src)
                //		});
                //	t = e.length > 0 ? e[0].src : document.scripts[document.scripts.length - 1].src
                //}
                if (typeof window !== 'undefined' && window && window.location && window.location.protocol && window.location.protocol !== 'https:') {
                    t = 'http://player.twitch.tv';
                }					
                var r = n.i(p.a)(t);
                return r.protocol + "://" + r.authority
            }(),
            b = Object.freeze({
                channelName: "",
                currentTime: 0,
                duration: 0,
                muted: !1,
                playback: "",
                quality: "",
                qualitiesAvailable: [],
                stats: {},
                videoID: "",
                viewers: 0,
                volume: 0
            }),
            g = Object.freeze({
                height: 390,
                width: 640,
                allowfullscreen: !1
            }),
            _ = "ready",
            m = "play",
            w = "pause",
            x = "ended",
            j = "online",
            O = "offline",
            E = "transitionToRecommendedVod",
            A = "minuteWatched",
            S = "videoPlay",
            k = "bufferEmpty",
            P = "videoError",
            T = "error",
            M = "play",
            L = "pause",
            R = "channel",
            I = "video",
            C = "collection",
            D = "seek",
            z = "quality",
            F = "mute",
            N = "volume",
            U = "theatre",
            V = "fullscreen",
            B = "setminiplayermode",
            W = "setcontent",
            q = "setclip",
            $ = "setTrackingProperties",
            H = "setPlayerType",
            Y = "enableCaptions",
            K = "disableCaptions",
            G = "subscribe",
            Z = "ready",
            Q = "paused",
            J = "ended",
            X = function() {
                function t(e) {
                    var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : g;
                    r(this, t), this._eventEmitter = new l.a, this._playerStateEmitter = new l.a, this._playerState = b, this._storeState = {}, this._onHostReady = this._getHostReady(), this._iframe = this._createPlayerIframe(n), e.appendChild(this._iframe), this._host = this._iframe.contentWindow, this._send(G)
                }
                return d(t, [{
                    key: "destroy",
                    value: function() {
                        this.callPlayerMethod("destroy")
                    }
                }, {
                    key: "_createPlayerIframe",
                    value: function(t) {
                        var e = this._normalizeOptions(t),
                            r = document.createElement("iframe"),
                            o = n.i(h.a)(a()(e, "width", "height")),
                            i = y + "/?" + o;
                        return r.setAttribute("src", i), r.setAttribute("width", e.width), r.setAttribute("height", e.height), r.setAttribute("frameBorder", "0"), r.setAttribute("scrolling", "no"), e.allowfullscreen && r.setAttribute("allowfullscreen", ""), r
                    }
                }, {
                    key: "_normalizeOptions",
                    value: function(t) {
                        var e = s()({}, g, t);
                        return !1 !== t.allowfullscreen && (e.allowfullscreen = !0), e
                    }
                }, {
                    key: "_getHostReady",
                    value: function() {
                        var t = this;
                        return new v.Promise(function(e, n) {
                            function r(t) {
                                this._isClientMessage(t) && t.data.method === Z && (this._storeState = t.data.args[0], window.removeEventListener("message", o), window.addEventListener("message", this), this._eventEmitter.emit(_), e())
                            }
                            var o = r.bind(t);
                            window.addEventListener("message", o), setTimeout(n, 15e3)
                        })
                    }
                }, {
                    key: "_send",
                    value: function(t) {
                        for (var e = arguments.length, n = Array(e > 1 ? e - 1 : 0), r = 1; r < e; r++) n[r - 1] = arguments[r];
                        var o = {
                            namespace: "player.embed.host",
                            method: t,
                            args: n
                        };
                        this._host.postMessage(o, "*")
                    }
                }, {
                    key: "_deferSend",
                    value: function() {
                        for (var t = this, e = arguments.length, n = Array(e), r = 0; r < e; r++) n[r] = arguments[r];
                        return this._onHostReady.then(function() {
                            return t._send.apply(t, n)
                        })
                    }
                }, {
                    key: "_isClientMessage",
                    value: function(t) {
                        return !!this._iframe && (Boolean(t.data) && "player.embed.client" === t.data.namespace && t.source === this._iframe.contentWindow)
                    }
                }, {
                    key: "handleEvent",
                    value: function(t) {
                        if (this._isClientMessage(t)) switch (t.data.method) {
                            case "bridgestateupdate":
                                this._playerState = t.data.args[0], this._playerStateEmitter.emit("playerstateupdate", this._playerState);
                                break;
                            case "bridgeplayerevent":
                                this._eventEmitter.emit(t.data.args[0]);
                                break;
                            case "bridgeplayereventwithpayload":
                                this._eventEmitter.emit(t.data.args[0].event, t.data.args[0].data);
                                break;
                            case "bridgestorestateupdate":
                                this._storeState = t.data.args[0];
                                break;
                            case "bridgedestroy":
                                this._iframe.parentNode.removeChild(this._iframe), delete this._iframe, delete this._host
                        }
                    }
                }, {
                    key: "getPlayerState",
                    value: function() {
                        return this._playerState
                    }
                }, {
                    key: "getStoreState",
                    value: function() {
                        return this._storeState
                    }
                }, {
                    key: "addEventListener",
                    value: function(t, e) {
                        this._eventEmitter.on(t, e)
                    }
                }, {
                    key: "addPlayerStateListener",
                    value: function(t) {
                        this._playerStateEmitter.on("playerstateupdate", t), t(this._playerState)
                    }
                }, {
                    key: "removePlayerStateListener",
                    value: function(t) {
                        this._playerStateEmitter.off("playerstateupdate", t)
                    }
                }, {
                    key: "removeEventListener",
                    value: function(t, e) {
                        this._eventEmitter.off(t, e)
                    }
                }, {
                    key: "callPlayerMethod",
                    value: function(t) {
                        for (var e = arguments.length, n = Array(e > 1 ? e - 1 : 0), r = 1; r < e; r++) n[r - 1] = arguments[r];
                        return this._deferSend.apply(this, [t].concat(n))
                    }
                }, {
                    key: "setWidth",
                    value: function(t) {
                        i()(t) && t >= 0 && this._iframe.setAttribute("width", t)
                    }
                }, {
                    key: "setHeight",
                    value: function(t) {
                        i()(t) && t >= 0 && this._iframe.setAttribute("height", t)
                    }
                }]), t
            }()
    }, function(t, e, n) {
        function r(t) {
            return "string" == typeof t || !i(t) && u(t) && o(t) == a
        }
        var o = n(2),
            i = n(1),
            u = n(6),
            a = "[object String]";
        t.exports = r
    }, function(t, e, n) {
        "use strict";

        function r(t) {
            for (var e = {
                    strictMode: !1,
                    key: ["source", "protocol", "authority", "userInfo", "user", "password", "host", "port", "relative", "path", "directory", "file", "query", "anchor"],
                    q: {
                        name: "queryKey",
                        parser: /(?:^|&)([^&=]*)=?([^&]*)/g
                    },
                    parser: {
                        strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
                        loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
                    }
                }, n = e.parser[e.strictMode ? "strict" : "loose"].exec(t), r = {}, o = 14; o--;) r[e.key[o]] = n[o] || "";
            return r[e.q.name] = {}, r[e.key[12]].replace(e.q.parser, function(t, n, o) {
                n && (r[e.q.name][n] = o)
            }), r
        }
        e.a = r
    }, function(t, e, n) {
        "use strict";

        function r(t) {
            var e = [];
            for (var n in t)
                if (t.hasOwnProperty(n)) {
                    var r = t[n];
                    n = encodeURIComponent(n), !0 === r ? e.push(n) : !1 === r ? e.push("!" + n) : null !== r && "object" === (void 0 === r ? "undefined" : o(r)) ? (r = encodeURIComponent(JSON.stringify(r)), e.push(n + "=" + r)) : (r = encodeURIComponent(r), e.push(n + "=" + r))
                }
            return e.join("&")
        }
        e.a = r;
        var o = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
            return typeof t
        } : function(t) {
            return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
        }
    }, function(t, e, n) {
        (function(e, r) {
            /*!
             * @overview es6-promise - a tiny implementation of Promises/A+.
             * @copyright Copyright (c) 2014 Yehuda Katz, Tom Dale, Stefan Penner and contributors (Conversion to ES6 API by Jake Archibald)
             * @license   Licensed under MIT license
             *            See https://raw.githubusercontent.com/stefanpenner/es6-promise/master/LICENSE
             * @version   3.3.1
             */
            ! function(e, n) {
                t.exports = n()
            }(0, function() {
                "use strict";

                function t(t) {
                    return "function" == typeof t || "object" == typeof t && null !== t
                }

                function o(t) {
                    return "function" == typeof t
                }

                function i(t) {
                    $ = t
                }

                function u(t) {
                    H = t
                }

                function a() {
                    return function() {
                        q(s)
                    }
                }

                function c() {
                    var t = setTimeout;
                    return function() {
                        return t(s, 1)
                    }
                }

                function s() {
                    for (var t = 0; t < W; t += 2) {
                        (0, J[t])(J[t + 1]), J[t] = void 0, J[t + 1] = void 0
                    }
                    W = 0
                }

                function f(t, e) {
                    var n = arguments,
                        r = this,
                        o = new this.constructor(p);
                    void 0 === o[tt] && M(o);
                    var i = r._state;
                    return i ? function() {
                        var t = n[i - 1];
                        H(function() {
                            return k(i, o, t, r._result)
                        })
                    }() : O(r, o, t, e), o
                }

                function l(t) {
                    var e = this;
                    if (t && "object" == typeof t && t.constructor === e) return t;
                    var n = new e(p);
                    return m(n, t), n
                }

                function p() {}

                function h() {
                    return new TypeError("You cannot resolve a promise with itself")
                }

                function v() {
                    return new TypeError("A promises callback cannot return that same promise.")
                }

                function d(t) {
                    try {
                        return t.then
                    } catch (t) {
                        return ot.error = t, ot
                    }
                }

                function y(t, e, n, r) {
                    try {
                        t.call(e, n, r)
                    } catch (t) {
                        return t
                    }
                }

                function b(t, e, n) {
                    H(function(t) {
                        var r = !1,
                            o = y(n, e, function(n) {
                                r || (r = !0, e !== n ? m(t, n) : x(t, n))
                            }, function(e) {
                                r || (r = !0, j(t, e))
                            }, "Settle: " + (t._label || " unknown promise"));
                        !r && o && (r = !0, j(t, o))
                    }, t)
                }

                function g(t, e) {
                    e._state === nt ? x(t, e._result) : e._state === rt ? j(t, e._result) : O(e, void 0, function(e) {
                        return m(t, e)
                    }, function(e) {
                        return j(t, e)
                    })
                }

                function _(t, e, n) {
                    e.constructor === t.constructor && n === f && e.constructor.resolve === l ? g(t, e) : n === ot ? j(t, ot.error) : void 0 === n ? x(t, e) : o(n) ? b(t, e, n) : x(t, e)
                }

                function m(e, n) {
                    e === n ? j(e, h()) : t(n) ? _(e, n, d(n)) : x(e, n)
                }

                function w(t) {
                    t._onerror && t._onerror(t._result), E(t)
                }

                function x(t, e) {
                    t._state === et && (t._result = e, t._state = nt, 0 !== t._subscribers.length && H(E, t))
                }

                function j(t, e) {
                    t._state === et && (t._state = rt, t._result = e, H(w, t))
                }

                function O(t, e, n, r) {
                    var o = t._subscribers,
                        i = o.length;
                    t._onerror = null, o[i] = e, o[i + nt] = n, o[i + rt] = r, 0 === i && t._state && H(E, t)
                }

                function E(t) {
                    var e = t._subscribers,
                        n = t._state;
                    if (0 !== e.length) {
                        for (var r = void 0, o = void 0, i = t._result, u = 0; u < e.length; u += 3) r = e[u], o = e[u + n], r ? k(n, r, o, i) : o(i);
                        t._subscribers.length = 0
                    }
                }

                function A() {
                    this.error = null
                }

                function S(t, e) {
                    try {
                        return t(e)
                    } catch (t) {
                        return it.error = t, it
                    }
                }

                function k(t, e, n, r) {
                    var i = o(n),
                        u = void 0,
                        a = void 0,
                        c = void 0,
                        s = void 0;
                    if (i) {
                        if (u = S(n, r), u === it ? (s = !0, a = u.error, u = null) : c = !0, e === u) return void j(e, v())
                    } else u = r, c = !0;
                    e._state !== et || (i && c ? m(e, u) : s ? j(e, a) : t === nt ? x(e, u) : t === rt && j(e, u))
                }

                function P(t, e) {
                    try {
                        e(function(e) {
                            m(t, e)
                        }, function(e) {
                            j(t, e)
                        })
                    } catch (e) {
                        j(t, e)
                    }
                }

                function T() {
                    return ut++
                }

                function M(t) {
                    t[tt] = ut++, t._state = void 0, t._result = void 0, t._subscribers = []
                }

                function L(t, e) {
                    this._instanceConstructor = t, this.promise = new t(p), this.promise[tt] || M(this.promise), B(e) ? (this._input = e, this.length = e.length, this._remaining = e.length, this._result = new Array(this.length), 0 === this.length ? x(this.promise, this._result) : (this.length = this.length || 0, this._enumerate(), 0 === this._remaining && x(this.promise, this._result))) : j(this.promise, R())
                }

                function R() {
                    return new Error("Array Methods must be provided an Array")
                }

                function I(t) {
                    return new L(this, t).promise
                }

                function C(t) {
                    var e = this;
                    return new e(B(t) ? function(n, r) {
                        for (var o = t.length, i = 0; i < o; i++) e.resolve(t[i]).then(n, r)
                    } : function(t, e) {
                        return e(new TypeError("You must pass an array to race."))
                    })
                }

                function D(t) {
                    var e = this,
                        n = new e(p);
                    return j(n, t), n
                }

                function z() {
                    throw new TypeError("You must pass a resolver function as the first argument to the promise constructor")
                }

                function F() {
                    throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.")
                }

                function N(t) {
                    this[tt] = T(), this._result = this._state = void 0, this._subscribers = [], p !== t && ("function" != typeof t && z(), this instanceof N ? P(this, t) : F())
                }

                function U() {
                    var t = void 0;
                    if (void 0 !== r) t = r;
                    else if ("undefined" != typeof self) t = self;
                    else try {
                        t = Function("return this")()
                    } catch (t) {
                        throw new Error("polyfill failed because global object is unavailable in this environment")
                    }
                    var e = t.Promise;
                    if (e) {
                        var n = null;
                        try {
                            n = Object.prototype.toString.call(e.resolve())
                        } catch (t) {}
                        if ("[object Promise]" === n && !e.cast) return
                    }
                    t.Promise = N
                }
                var V = void 0;
                V = Array.isArray ? Array.isArray : function(t) {
                    return "[object Array]" === Object.prototype.toString.call(t)
                };
                var B = V,
                    W = 0,
                    q = void 0,
                    $ = void 0,
                    H = function(t, e) {
                        J[W] = t, J[W + 1] = e, 2 === (W += 2) && ($ ? $(s) : X())
                    },
                    Y = "undefined" != typeof window ? window : void 0,
                    K = Y || {},
                    G = K.MutationObserver || K.WebKitMutationObserver,
                    Z = "undefined" == typeof self && void 0 !== e && "[object process]" === {}.toString.call(e),
                    Q = "undefined" != typeof Uint8ClampedArray && "undefined" != typeof importScripts && "undefined" != typeof MessageChannel,
                    J = new Array(1e3),
                    X = void 0;
                X = Z ? function() {
                    return function() {
                        return e.nextTick(s)
                    }
                }() : G ? function() {
                    var t = 0,
                        e = new G(s),
                        n = document.createTextNode("");
                    return e.observe(n, {
                            characterData: !0
                        }),
                        function() {
                            n.data = t = ++t % 2
                        }
                }() : Q ? function() {
                    var t = new MessageChannel;
                    return t.port1.onmessage = s,
                        function() {
                            return t.port2.postMessage(0)
                        }
                }() : void 0 === Y ? function() {
                    try {
                        var t = n(153);
                        return q = t.runOnLoop || t.runOnContext, a()
                    } catch (t) {
                        return c()
                    }
                }() : c();
                var tt = Math.random().toString(36).substring(16),
                    et = void 0,
                    nt = 1,
                    rt = 2,
                    ot = new A,
                    it = new A,
                    ut = 0;
                return L.prototype._enumerate = function() {
                    for (var t = this.length, e = this._input, n = 0; this._state === et && n < t; n++) this._eachEntry(e[n], n)
                }, L.prototype._eachEntry = function(t, e) {
                    var n = this._instanceConstructor,
                        r = n.resolve;
                    if (r === l) {
                        var o = d(t);
                        if (o === f && t._state !== et) this._settledAt(t._state, e, t._result);
                        else if ("function" != typeof o) this._remaining--, this._result[e] = t;
                        else if (n === N) {
                            var i = new n(p);
                            _(i, t, o), this._willSettleAt(i, e)
                        } else this._willSettleAt(new n(function(e) {
                            return e(t)
                        }), e)
                    } else this._willSettleAt(r(t), e)
                }, L.prototype._settledAt = function(t, e, n) {
                    var r = this.promise;
                    r._state === et && (this._remaining--, t === rt ? j(r, n) : this._result[e] = n), 0 === this._remaining && x(r, this._result)
                }, L.prototype._willSettleAt = function(t, e) {
                    var n = this;
                    O(t, void 0, function(t) {
                        return n._settledAt(nt, e, t)
                    }, function(t) {
                        return n._settledAt(rt, e, t)
                    })
                }, N.all = I, N.race = C, N.resolve = l, N.reject = D, N._setScheduler = i, N._setAsap = u, N._asap = H, N.prototype = {
                    constructor: N,
                    then: f,
                    catch: function(t) {
                        return this.then(null, t)
                    }
                }, U(), N.polyfill = U, N.Promise = N, N
            })
        }).call(e, n(151), n(48))
    }, function(t, e, n) {
        var r = n(3),
            o = n(0),
            i = r(o, "DataView");
        t.exports = i
    }, function(t, e, n) {
        function r(t) {
            var e = -1,
                n = null == t ? 0 : t.length;
            for (this.clear(); ++e < n;) {
                var r = t[e];
                this.set(r[0], r[1])
            }
        }
        var o = n(102),
            i = n(103),
            u = n(104),
            a = n(105),
            c = n(106);
        r.prototype.clear = o, r.prototype.delete = i, r.prototype.get = u, r.prototype.has = a, r.prototype.set = c, t.exports = r
    }, function(t, e, n) {
        var r = n(3),
            o = n(0),
            i = r(o, "Promise");
        t.exports = i
    }, function(t, e, n) {
        var r = n(3),
            o = n(0),
            i = r(o, "Set");
        t.exports = i
    }, function(t, e, n) {
        function r(t) {
            var e = this.__data__ = new o(t);
            this.size = e.size
        }
        var o = n(8),
            i = n(134),
            u = n(135),
            a = n(136),
            c = n(137),
            s = n(138);
        r.prototype.clear = i, r.prototype.delete = u, r.prototype.get = a, r.prototype.has = c, r.prototype.set = s, t.exports = r
    }, function(t, e, n) {
        var r = n(0),
            o = r.Uint8Array;
        t.exports = o
    }, function(t, e, n) {
        var r = n(3),
            o = n(0),
            i = r(o, "WeakMap");
        t.exports = i
    }, function(t, e) {
        function n(t, e) {
            return t.set(e[0], e[1]), t
        }
        t.exports = n
    }, function(t, e) {
        function n(t, e) {
            return t.add(e), t
        }
        t.exports = n
    }, function(t, e) {
        function n(t, e, n) {
            switch (n.length) {
                case 0:
                    return t.call(e);
                case 1:
                    return t.call(e, n[0]);
                case 2:
                    return t.call(e, n[0], n[1]);
                case 3:
                    return t.call(e, n[0], n[1], n[2])
            }
            return t.apply(e, n)
        }
        t.exports = n
    }, function(t, e) {
        function n(t, e) {
            for (var n = -1, r = null == t ? 0 : t.length; ++n < r && !1 !== e(t[n], n, t););
            return t
        }
        t.exports = n
    }, function(t, e) {
        function n(t, e) {
            for (var n = -1, r = null == t ? 0 : t.length, o = 0, i = []; ++n < r;) {
                var u = t[n];
                e(u, n, t) && (i[o++] = u)
            }
            return i
        }
        t.exports = n
    }, function(t, e, n) {
        function r(t, e) {
            return t && o(e, i(e), t)
        }
        var o = n(4),
            i = n(14);
        t.exports = r
    }, function(t, e, n) {
        function r(t, e) {
            return t && o(e, i(e), t)
        }
        var o = n(4),
            i = n(46);
        t.exports = r
    }, function(t, e, n) {
        function r(t, e, n, M, L, R) {
            var I, C = e & j,
                D = e & O,
                z = e & E;
            if (n && (I = L ? n(t, M, L, R) : n(t)), void 0 !== I) return I;
            if (!w(t)) return t;
            var F = _(t);
            if (F) {
                if (I = y(t), !C) return f(t, I)
            } else {
                var N = d(t),
                    U = N == S || N == k;
                if (m(t)) return s(t, C);
                if (N == P || N == A || U && !L) {
                    if (I = D || U ? {} : g(t), !C) return D ? p(t, c(I, t)) : l(t, a(I, t))
                } else {
                    if (!T[N]) return L ? t : {};
                    I = b(t, N, r, C)
                }
            }
            R || (R = new o);
            var V = R.get(t);
            if (V) return V;
            R.set(t, I);
            var B = z ? D ? v : h : D ? keysIn : x,
                W = F ? void 0 : B(t);
            return i(W || t, function(o, i) {
                W && (i = o, o = t[i]), u(I, i, r(o, e, n, i, t, R))
            }), I
        }
        var o = n(58),
            i = n(64),
            u = n(17),
            a = n(66),
            c = n(67),
            s = n(84),
            f = n(91),
            l = n(92),
            p = n(93),
            h = n(98),
            v = n(33),
            d = n(100),
            y = n(107),
            b = n(108),
            g = n(109),
            _ = n(1),
            m = n(43),
            w = n(5),
            x = n(14),
            j = 1,
            O = 2,
            E = 4,
            A = "[object Arguments]",
            S = "[object Function]",
            k = "[object GeneratorFunction]",
            P = "[object Object]",
            T = {};
        T[A] = T["[object Array]"] = T["[object ArrayBuffer]"] = T["[object DataView]"] = T["[object Boolean]"] = T["[object Date]"] = T["[object Float32Array]"] = T["[object Float64Array]"] = T["[object Int8Array]"] = T["[object Int16Array]"] = T["[object Int32Array]"] = T["[object Map]"] = T["[object Number]"] = T[P] = T["[object RegExp]"] = T["[object Set]"] = T["[object String]"] = T["[object Symbol]"] = T["[object Uint8Array]"] = T["[object Uint8ClampedArray]"] = T["[object Uint16Array]"] = T["[object Uint32Array]"] = !0, T["[object Error]"] = T[S] = T["[object WeakMap]"] = !1, t.exports = r
    }, function(t, e, n) {
        var r = n(5),
            o = Object.create,
            i = function() {
                function t() {}
                return function(e) {
                    if (!r(e)) return {};
                    if (o) return o(e);
                    t.prototype = e;
                    var n = new t;
                    return t.prototype = void 0, n
                }
            }();
        t.exports = i
    }, function(t, e, n) {
        function r(t, e, n, u, a) {
            var c = -1,
                s = t.length;
            for (n || (n = i), a || (a = []); ++c < s;) {
                var f = t[c];
                e > 0 && n(f) ? e > 1 ? r(f, e - 1, n, u, a) : o(a, f) : u || (a[a.length] = f)
            }
            return a
        }
        var o = n(16),
            i = n(110);
        t.exports = r
    }, function(t, e, n) {
        function r(t, e) {
            e = o(e, t);
            for (var n = 0, r = e.length; null != t && n < r;) t = t[i(e[n++])];
            return n && n == r ? t : void 0
        }
        var o = n(18),
            i = n(39);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return i(t) && o(t) == u
        }
        var o = n(2),
            i = n(6),
            u = "[object Arguments]";
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return !(!u(t) || i(t)) && (o(t) ? v : s).test(a(t))
        }
        var o = n(44),
            i = n(114),
            u = n(5),
            a = n(40),
            c = /[\\^$.*+?()[\]{}|]/g,
            s = /^\[object .+?Constructor\]$/,
            f = Function.prototype,
            l = Object.prototype,
            p = f.toString,
            h = l.hasOwnProperty,
            v = RegExp("^" + p.call(h).replace(c, "\\$&").replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, "$1.*?") + "$");
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return u(t) && i(t.length) && !!a[o(t)]
        }
        var o = n(2),
            i = n(45),
            u = n(6),
            a = {};
        a["[object Float32Array]"] = a["[object Float64Array]"] = a["[object Int8Array]"] = a["[object Int16Array]"] = a["[object Int32Array]"] = a["[object Uint8Array]"] = a["[object Uint8ClampedArray]"] = a["[object Uint16Array]"] = a["[object Uint32Array]"] = !0, a["[object Arguments]"] = a["[object Array]"] = a["[object ArrayBuffer]"] = a["[object Boolean]"] = a["[object DataView]"] = a["[object Date]"] = a["[object Error]"] = a["[object Function]"] = a["[object Map]"] = a["[object Number]"] = a["[object Object]"] = a["[object RegExp]"] = a["[object Set]"] = a["[object String]"] = a["[object WeakMap]"] = !1, t.exports = r
    }, function(t, e, n) {
        function r(t) {
            if (!o(t)) return i(t);
            var e = [];
            for (var n in Object(t)) a.call(t, n) && "constructor" != n && e.push(n);
            return e
        }
        var o = n(11),
            i = n(127),
            u = Object.prototype,
            a = u.hasOwnProperty;
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            if (!o(t)) return u(t);
            var e = i(t),
                n = [];
            for (var r in t)("constructor" != r || !e && c.call(t, r)) && n.push(r);
            return n
        }
        var o = n(5),
            i = n(11),
            u = n(128),
            a = Object.prototype,
            c = a.hasOwnProperty;
        t.exports = r
    }, function(t, e, n) {
        function r(t, e) {
            return u(i(t, e, o), t + "")
        }
        var o = n(41),
            i = n(37),
            u = n(38);
        t.exports = r
    }, function(t, e, n) {
        var r = n(141),
            o = n(31),
            i = n(41),
            u = o ? function(t, e) {
                return o(t, "toString", {
                    configurable: !0,
                    enumerable: !1,
                    value: r(e),
                    writable: !0
                })
            } : i;
        t.exports = u
    }, function(t, e) {
        function n(t, e, n) {
            var r = -1,
                o = t.length;
            e < 0 && (e = -e > o ? 0 : o + e), n = n > o ? o : n, n < 0 && (n += o), o = e > n ? 0 : n - e >>> 0, e >>>= 0;
            for (var i = Array(o); ++r < o;) i[r] = t[r + e];
            return i
        }
        t.exports = n
    }, function(t, e) {
        function n(t, e) {
            for (var n = -1, r = Array(t); ++n < t;) r[n] = e(n);
            return r
        }
        t.exports = n
    }, function(t, e, n) {
        function r(t) {
            if ("string" == typeof t) return t;
            if (u(t)) return i(t, r) + "";
            if (a(t)) return f ? f.call(t) : "";
            var e = t + "";
            return "0" == e && 1 / t == -c ? "-0" : e
        }
        var o = n(7),
            i = n(27),
            u = n(1),
            a = n(23),
            c = 1 / 0,
            s = o ? o.prototype : void 0,
            f = s ? s.toString : void 0;
        t.exports = r
    }, function(t, e) {
        function n(t) {
            return function(e) {
                return t(e)
            }
        }
        t.exports = n
    }, function(t, e, n) {
        function r(t, e) {
            return e = o(e, t), null == (t = u(t, e)) || delete t[a(i(e))]
        }
        var o = n(18),
            i = n(146),
            u = n(131),
            a = n(39);
        t.exports = r
    }, function(t, e, n) {
        (function(t) {
            function r(t, e) {
                if (e) return t.slice();
                var n = t.length,
                    r = s ? s(n) : new t.constructor(n);
                return t.copy(r), r
            }
            var o = n(0),
                i = "object" == typeof e && e && !e.nodeType && e,
                u = i && "object" == typeof t && t && !t.nodeType && t,
                a = u && u.exports === i,
                c = a ? o.Buffer : void 0,
                s = c ? c.allocUnsafe : void 0;
            t.exports = r
        }).call(e, n(24)(t))
    }, function(t, e, n) {
        function r(t, e) {
            var n = e ? o(t.buffer) : t.buffer;
            return new t.constructor(n, t.byteOffset, t.byteLength)
        }
        var o = n(19);
        t.exports = r
    }, function(t, e, n) {
        function r(t, e, n) {
            var r = e ? n(u(t), a) : u(t);
            return i(r, o, new t.constructor)
        }
        var o = n(61),
            i = n(28),
            u = n(125),
            a = 1;
        t.exports = r
    }, function(t, e) {
        function n(t) {
            var e = new t.constructor(t.source, r.exec(t));
            return e.lastIndex = t.lastIndex, e
        }
        var r = /\w*$/;
        t.exports = n
    }, function(t, e, n) {
        function r(t, e, n) {
            var r = e ? n(u(t), a) : u(t);
            return i(r, o, new t.constructor)
        }
        var o = n(62),
            i = n(28),
            u = n(132),
            a = 1;
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return u ? Object(u.call(t)) : {}
        }
        var o = n(7),
            i = o ? o.prototype : void 0,
            u = i ? i.valueOf : void 0;
        t.exports = r
    }, function(t, e, n) {
        function r(t, e) {
            var n = e ? o(t.buffer) : t.buffer;
            return new t.constructor(n, t.byteOffset, t.length)
        }
        var o = n(19);
        t.exports = r
    }, function(t, e) {
        function n(t, e) {
            var n = -1,
                r = t.length;
            for (e || (e = Array(r)); ++n < r;) e[n] = t[n];
            return e
        }
        t.exports = n
    }, function(t, e, n) {
        function r(t, e) {
            return o(t, i(t), e)
        }
        var o = n(4),
            i = n(21);
        t.exports = r
    }, function(t, e, n) {
        function r(t, e) {
            return o(t, i(t), e)
        }
        var o = n(4),
            i = n(34);
        t.exports = r
    }, function(t, e, n) {
        var r = n(0),
            o = r["__core-js_shared__"];
        t.exports = o
    }, function(t, e, n) {
        function r(t) {
            return o(function(e, n) {
                var r = -1,
                    o = n.length,
                    u = o > 1 ? n[o - 1] : void 0,
                    a = o > 2 ? n[2] : void 0;
                for (u = t.length > 3 && "function" == typeof u ? (o--, u) : void 0, a && i(n[0], n[1], a) && (u = o < 3 ? void 0 : u, o = 1), e = Object(e); ++r < o;) {
                    var c = n[r];
                    c && t(e, c, r, u)
                }
                return e
            })
        }
        var o = n(77),
            i = n(111);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return o(t) ? void 0 : t
        }
        var o = n(144);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return u(i(t, void 0, o), t + "")
        }
        var o = n(142),
            i = n(37),
            u = n(38);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return o(t, u, i)
        }
        var o = n(30),
            i = n(21),
            u = n(14);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            var e = u.call(t, c),
                n = t[c];
            try {
                t[c] = void 0;
                var r = !0
            } catch (t) {}
            var o = a.call(t);
            return r && (e ? t[c] = n : delete t[c]), o
        }
        var o = n(7),
            i = Object.prototype,
            u = i.hasOwnProperty,
            a = i.toString,
            c = o ? o.toStringTag : void 0;
        t.exports = r
    }, function(t, e, n) {
        var r = n(54),
            o = n(15),
            i = n(56),
            u = n(57),
            a = n(60),
            c = n(2),
            s = n(40),
            f = s(r),
            l = s(o),
            p = s(i),
            h = s(u),
            v = s(a),
            d = c;
        (r && "[object DataView]" != d(new r(new ArrayBuffer(1))) || o && "[object Map]" != d(new o) || i && "[object Promise]" != d(i.resolve()) || u && "[object Set]" != d(new u) || a && "[object WeakMap]" != d(new a)) && (d = function(t) {
            var e = c(t),
                n = "[object Object]" == e ? t.constructor : void 0,
                r = n ? s(n) : "";
            if (r) switch (r) {
                case f:
                    return "[object DataView]";
                case l:
                    return "[object Map]";
                case p:
                    return "[object Promise]";
                case h:
                    return "[object Set]";
                case v:
                    return "[object WeakMap]"
            }
            return e
        }), t.exports = d
    }, function(t, e) {
        function n(t, e) {
            return null == t ? void 0 : t[e]
        }
        t.exports = n
    }, function(t, e, n) {
        function r() {
            this.__data__ = o ? o(null) : {}, this.size = 0
        }
        var o = n(12);
        t.exports = r
    }, function(t, e) {
        function n(t) {
            var e = this.has(t) && delete this.__data__[t];
            return this.size -= e ? 1 : 0, e
        }
        t.exports = n
    }, function(t, e, n) {
        function r(t) {
            var e = this.__data__;
            if (o) {
                var n = e[t];
                return n === i ? void 0 : n
            }
            return a.call(e, t) ? e[t] : void 0
        }
        var o = n(12),
            i = "__lodash_hash_undefined__",
            u = Object.prototype,
            a = u.hasOwnProperty;
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            var e = this.__data__;
            return o ? void 0 !== e[t] : u.call(e, t)
        }
        var o = n(12),
            i = Object.prototype,
            u = i.hasOwnProperty;
        t.exports = r
    }, function(t, e, n) {
        function r(t, e) {
            var n = this.__data__;
            return this.size += this.has(t) ? 0 : 1, n[t] = o && void 0 === e ? i : e, this
        }
        var o = n(12),
            i = "__lodash_hash_undefined__";
        t.exports = r
    }, function(t, e) {
        function n(t) {
            var e = t.length,
                n = t.constructor(e);
            return e && "string" == typeof t[0] && o.call(t, "index") && (n.index = t.index, n.input = t.input), n
        }
        var r = Object.prototype,
            o = r.hasOwnProperty;
        t.exports = n
    }, function(t, e, n) {
        function r(t, e, n, r) {
            var T = t.constructor;
            switch (e) {
                case _:
                    return o(t);
                case l:
                case p:
                    return new T(+t);
                case m:
                    return i(t, r);
                case w:
                case x:
                case j:
                case O:
                case E:
                case A:
                case S:
                case k:
                case P:
                    return f(t, r);
                case h:
                    return u(t, r, n);
                case v:
                case b:
                    return new T(t);
                case d:
                    return a(t);
                case y:
                    return c(t, r, n);
                case g:
                    return s(t)
            }
        }
        var o = n(19),
            i = n(85),
            u = n(86),
            a = n(87),
            c = n(88),
            s = n(89),
            f = n(90),
            l = "[object Boolean]",
            p = "[object Date]",
            h = "[object Map]",
            v = "[object Number]",
            d = "[object RegExp]",
            y = "[object Set]",
            b = "[object String]",
            g = "[object Symbol]",
            _ = "[object ArrayBuffer]",
            m = "[object DataView]",
            w = "[object Float32Array]",
            x = "[object Float64Array]",
            j = "[object Int8Array]",
            O = "[object Int16Array]",
            E = "[object Int32Array]",
            A = "[object Uint8Array]",
            S = "[object Uint8ClampedArray]",
            k = "[object Uint16Array]",
            P = "[object Uint32Array]";
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return "function" != typeof t.constructor || u(t) ? {} : o(i(t))
        }
        var o = n(69),
            i = n(20),
            u = n(11);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return u(t) || i(t) || !!(a && t && t[a])
        }
        var o = n(7),
            i = n(42),
            u = n(1),
            a = o ? o.isConcatSpreadable : void 0;
        t.exports = r
    }, function(t, e, n) {
        function r(t, e, n) {
            if (!a(n)) return !1;
            var r = typeof e;
            return !!("number" == r ? i(n) && u(e, n.length) : "string" == r && e in n) && o(n[e], t)
        }
        var o = n(22),
            i = n(13),
            u = n(35),
            a = n(5);
        t.exports = r
    }, function(t, e, n) {
        function r(t, e) {
            if (o(t)) return !1;
            var n = typeof t;
            return !("number" != n && "symbol" != n && "boolean" != n && null != t && !i(t)) || (a.test(t) || !u.test(t) || null != e && t in Object(e))
        }
        var o = n(1),
            i = n(23),
            u = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
            a = /^\w*$/;
        t.exports = r
    }, function(t, e) {
        function n(t) {
            var e = typeof t;
            return "string" == e || "number" == e || "symbol" == e || "boolean" == e ? "__proto__" !== t : null === t
        }
        t.exports = n
    }, function(t, e, n) {
        function r(t) {
            return !!i && i in t
        }
        var o = n(94),
            i = function() {
                var t = /[^.]+$/.exec(o && o.keys && o.keys.IE_PROTO || "");
                return t ? "Symbol(src)_1." + t : ""
            }();
        t.exports = r
    }, function(t, e) {
        function n() {
            this.__data__ = [], this.size = 0
        }
        t.exports = n
    }, function(t, e, n) {
        function r(t) {
            var e = this.__data__,
                n = o(e, t);
            return !(n < 0) && (n == e.length - 1 ? e.pop() : u.call(e, n, 1), --this.size, !0)
        }
        var o = n(9),
            i = Array.prototype,
            u = i.splice;
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            var e = this.__data__,
                n = o(e, t);
            return n < 0 ? void 0 : e[n][1]
        }
        var o = n(9);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return o(this.__data__, t) > -1
        }
        var o = n(9);
        t.exports = r
    }, function(t, e, n) {
        function r(t, e) {
            var n = this.__data__,
                r = o(n, t);
            return r < 0 ? (++this.size, n.push([t, e])) : n[r][1] = e, this
        }
        var o = n(9);
        t.exports = r
    }, function(t, e, n) {
        function r() {
            this.size = 0, this.__data__ = {
                hash: new o,
                map: new(u || i),
                string: new o
            }
        }
        var o = n(55),
            i = n(8),
            u = n(15);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            var e = o(this, t).delete(t);
            return this.size -= e ? 1 : 0, e
        }
        var o = n(10);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return o(this, t).get(t)
        }
        var o = n(10);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return o(this, t).has(t)
        }
        var o = n(10);
        t.exports = r
    }, function(t, e, n) {
        function r(t, e) {
            var n = o(this, t),
                r = n.size;
            return n.set(t, e), this.size += n.size == r ? 0 : 1, this
        }
        var o = n(10);
        t.exports = r
    }, function(t, e) {
        function n(t) {
            var e = -1,
                n = Array(t.size);
            return t.forEach(function(t, r) {
                n[++e] = [r, t]
            }), n
        }
        t.exports = n
    }, function(t, e, n) {
        function r(t) {
            var e = o(t, function(t) {
                    return n.size === i && n.clear(), t
                }),
                n = e.cache;
            return e
        }
        var o = n(147),
            i = 500;
        t.exports = r
    }, function(t, e, n) {
        var r = n(36),
            o = r(Object.keys, Object);
        t.exports = o
    }, function(t, e) {
        function n(t) {
            var e = [];
            if (null != t)
                for (var n in Object(t)) e.push(n);
            return e
        }
        t.exports = n
    }, function(t, e, n) {
        (function(t) {
            var r = n(32),
                o = "object" == typeof e && e && !e.nodeType && e,
                i = o && "object" == typeof t && t && !t.nodeType && t,
                u = i && i.exports === o,
                a = u && r.process,
                c = function() {
                    try {
                        return a && a.binding && a.binding("util")
                    } catch (t) {}
                }();
            t.exports = c
        }).call(e, n(24)(t))
    }, function(t, e) {
        function n(t) {
            return o.call(t)
        }
        var r = Object.prototype,
            o = r.toString;
        t.exports = n
    }, function(t, e, n) {
        function r(t, e) {
            return e.length < 2 ? t : o(t, i(e, 0, -1))
        }
        var o = n(71),
            i = n(79);
        t.exports = r
    }, function(t, e) {
        function n(t) {
            var e = -1,
                n = Array(t.size);
            return t.forEach(function(t) {
                n[++e] = t
            }), n
        }
        t.exports = n
    }, function(t, e) {
        function n(t) {
            var e = 0,
                n = 0;
            return function() {
                var u = i(),
                    a = o - (u - n);
                if (n = u, a > 0) {
                    if (++e >= r) return arguments[0]
                } else e = 0;
                return t.apply(void 0, arguments)
            }
        }
        var r = 800,
            o = 16,
            i = Date.now;
        t.exports = n
    }, function(t, e, n) {
        function r() {
            this.__data__ = new o, this.size = 0
        }
        var o = n(8);
        t.exports = r
    }, function(t, e) {
        function n(t) {
            var e = this.__data__,
                n = e.delete(t);
            return this.size = e.size, n
        }
        t.exports = n
    }, function(t, e) {
        function n(t) {
            return this.__data__.get(t)
        }
        t.exports = n
    }, function(t, e) {
        function n(t) {
            return this.__data__.has(t)
        }
        t.exports = n
    }, function(t, e, n) {
        function r(t, e) {
            var n = this.__data__;
            if (n instanceof o) {
                var r = n.__data__;
                if (!i || r.length < a - 1) return r.push([t, e]), this.size = ++n.size, this;
                n = this.__data__ = new u(r)
            }
            return n.set(t, e), this.size = n.size, this
        }
        var o = n(8),
            i = n(15),
            u = n(25),
            a = 200;
        t.exports = r
    }, function(t, e, n) {
        var r = n(126),
            o = /^\./,
            i = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g,
            u = /\\(\\)?/g,
            a = r(function(t) {
                var e = [];
                return o.test(t) && e.push(""), t.replace(i, function(t, n, r, o) {
                    e.push(r ? o.replace(u, "$1") : n || t)
                }), e
            });
        t.exports = a
    }, function(t, e, n) {
        var r = n(17),
            o = n(4),
            i = n(95),
            u = n(13),
            a = n(11),
            c = n(14),
            s = Object.prototype,
            f = s.hasOwnProperty,
            l = i(function(t, e) {
                if (a(e) || u(e)) return void o(e, c(e), t);
                for (var n in e) f.call(e, n) && r(t, n, e[n])
            });
        t.exports = l
    }, function(t, e) {
        function n(t) {
            return function() {
                return t
            }
        }
        t.exports = n
    }, function(t, e, n) {
        function r(t) {
            return (null == t ? 0 : t.length) ? o(t, 1) : []
        }
        var o = n(70);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return "number" == typeof t && i(t)
        }
        var o = n(0),
            i = o.isFinite;
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            if (!u(t) || o(t) != a) return !1;
            var e = i(t);
            if (null === e) return !0;
            var n = l.call(e, "constructor") && e.constructor;
            return "function" == typeof n && n instanceof n && f.call(n) == p
        }
        var o = n(2),
            i = n(20),
            u = n(6),
            a = "[object Object]",
            c = Function.prototype,
            s = Object.prototype,
            f = c.toString,
            l = s.hasOwnProperty,
            p = f.call(Object);
        t.exports = r
    }, function(t, e, n) {
        var r = n(74),
            o = n(82),
            i = n(129),
            u = i && i.isTypedArray,
            a = u ? o(u) : r;
        t.exports = a
    }, function(t, e) {
        function n(t) {
            var e = null == t ? 0 : t.length;
            return e ? t[e - 1] : void 0
        }
        t.exports = n
    }, function(t, e, n) {
        function r(t, e) {
            if ("function" != typeof t || null != e && "function" != typeof e) throw new TypeError(i);
            var n = function() {
                var r = arguments,
                    o = e ? e.apply(this, r) : r[0],
                    i = n.cache;
                if (i.has(o)) return i.get(o);
                var u = t.apply(this, r);
                return n.cache = i.set(o, u) || i, u
            };
            return n.cache = new(r.Cache || o), n
        }
        var o = n(25),
            i = "Expected a function";
        r.Cache = o, t.exports = r
    }, function(t, e, n) {
        var r = n(27),
            o = n(68),
            i = n(83),
            u = n(18),
            a = n(4),
            c = n(96),
            s = n(97),
            f = n(33),
            l = s(function(t, e) {
                var n = {};
                if (null == t) return n;
                var s = !1;
                e = r(e, function(e) {
                    return e = u(e, t), s || (s = e.length > 1), e
                }), a(t, f(t), n), s && (n = o(n, 7, c));
                for (var l = e.length; l--;) i(n, e[l]);
                return n
            });
        t.exports = l
    }, function(t, e) {
        function n() {
            return !1
        }
        t.exports = n
    }, function(t, e, n) {
        function r(t) {
            return null == t ? "" : o(t)
        }
        var o = n(81);
        t.exports = r
    }, function(t, e) {
        function n() {
            throw new Error("setTimeout has not been defined")
        }

        function r() {
            throw new Error("clearTimeout has not been defined")
        }

        function o(t) {
            if (f === setTimeout) return setTimeout(t, 0);
            if ((f === n || !f) && setTimeout) return f = setTimeout, setTimeout(t, 0);
            try {
                return f(t, 0)
            } catch (e) {
                try {
                    return f.call(null, t, 0)
                } catch (e) {
                    return f.call(this, t, 0)
                }
            }
        }

        function i(t) {
            if (l === clearTimeout) return clearTimeout(t);
            if ((l === r || !l) && clearTimeout) return l = clearTimeout, clearTimeout(t);
            try {
                return l(t)
            } catch (e) {
                try {
                    return l.call(null, t)
                } catch (e) {
                    return l.call(this, t)
                }
            }
        }

        function u() {
            d && h && (d = !1, h.length ? v = h.concat(v) : y = -1, v.length && a())
        }

        function a() {
            if (!d) {
                var t = o(u);
                d = !0;
                for (var e = v.length; e;) {
                    for (h = v, v = []; ++y < e;) h && h[y].run();
                    y = -1, e = v.length
                }
                h = null, d = !1, i(t)
            }
        }

        function c(t, e) {
            this.fun = t, this.array = e
        }

        function s() {}
        var f, l, p = t.exports = {};
        ! function() {
            try {
                f = "function" == typeof setTimeout ? setTimeout : n
            } catch (t) {
                f = n
            }
            try {
                l = "function" == typeof clearTimeout ? clearTimeout : r
            } catch (t) {
                l = r
            }
        }();
        var h, v = [],
            d = !1,
            y = -1;
        p.nextTick = function(t) {
            var e = new Array(arguments.length - 1);
            if (arguments.length > 1)
                for (var n = 1; n < arguments.length; n++) e[n - 1] = arguments[n];
            v.push(new c(t, e)), 1 !== v.length || d || o(a)
        }, c.prototype.run = function() {
            this.fun.apply(null, this.array)
        }, p.title = "browser", p.browser = !0, p.env = {}, p.argv = [], p.version = "", p.versions = {}, p.on = s, p.addListener = s, p.once = s, p.off = s, p.removeListener = s, p.removeAllListeners = s, p.emit = s, p.prependListener = s, p.prependOnceListener = s, p.listeners = function(t) {
            return []
        }, p.binding = function(t) {
            throw new Error("process.binding is not supported")
        }, p.cwd = function() {
            return "/"
        }, p.chdir = function(t) {
            throw new Error("process.chdir is not supported")
        }, p.umask = function() {
            return 0
        }
    }, function(t, e, n) {
        var r;
        (function() {
            "use strict";

            function e() {}

            function o(t, e) {
                for (var n = t.length; n--;)
                    if (t[n].listener === e) return n;
                return -1
            }

            function i(t) {
                return function() {
                    return this[t].apply(this, arguments)
                }
            }
            var u = e.prototype,
                a = this,
                c = a.EventEmitter;
            u.getListeners = function(t) {
                var e, n, r = this._getEvents();
                if (t instanceof RegExp) {
                    e = {};
                    for (n in r) r.hasOwnProperty(n) && t.test(n) && (e[n] = r[n])
                } else e = r[t] || (r[t] = []);
                return e
            }, u.flattenListeners = function(t) {
                var e, n = [];
                for (e = 0; e < t.length; e += 1) n.push(t[e].listener);
                return n
            }, u.getListenersAsObject = function(t) {
                var e, n = this.getListeners(t);
                return n instanceof Array && (e = {}, e[t] = n), e || n
            }, u.addListener = function(t, e) {
                var n, r = this.getListenersAsObject(t),
                    i = "object" == typeof e;
                for (n in r) r.hasOwnProperty(n) && -1 === o(r[n], e) && r[n].push(i ? e : {
                    listener: e,
                    once: !1
                });
                return this
            }, u.on = i("addListener"), u.addOnceListener = function(t, e) {
                return this.addListener(t, {
                    listener: e,
                    once: !0
                })
            }, u.once = i("addOnceListener"), u.defineEvent = function(t) {
                return this.getListeners(t), this
            }, u.defineEvents = function(t) {
                for (var e = 0; e < t.length; e += 1) this.defineEvent(t[e]);
                return this
            }, u.removeListener = function(t, e) {
                var n, r, i = this.getListenersAsObject(t);
                for (r in i) i.hasOwnProperty(r) && -1 !== (n = o(i[r], e)) && i[r].splice(n, 1);
                return this
            }, u.off = i("removeListener"), u.addListeners = function(t, e) {
                return this.manipulateListeners(!1, t, e)
            }, u.removeListeners = function(t, e) {
                return this.manipulateListeners(!0, t, e)
            }, u.manipulateListeners = function(t, e, n) {
                var r, o, i = t ? this.removeListener : this.addListener,
                    u = t ? this.removeListeners : this.addListeners;
                if ("object" != typeof e || e instanceof RegExp)
                    for (r = n.length; r--;) i.call(this, e, n[r]);
                else
                    for (r in e) e.hasOwnProperty(r) && (o = e[r]) && ("function" == typeof o ? i.call(this, r, o) : u.call(this, r, o));
                return this
            }, u.removeEvent = function(t) {
                var e, n = typeof t,
                    r = this._getEvents();
                if ("string" === n) delete r[t];
                else if (t instanceof RegExp)
                    for (e in r) r.hasOwnProperty(e) && t.test(e) && delete r[e];
                else delete this._events;
                return this
            }, u.removeAllListeners = i("removeEvent"), u.emitEvent = function(t, e) {
                var n, r, o, i, u = this.getListenersAsObject(t);
                for (i in u)
                    if (u.hasOwnProperty(i))
                        for (n = u[i].slice(0), o = n.length; o--;) r = n[o], !0 === r.once && this.removeListener(t, r.listener), r.listener.apply(this, e || []) === this._getOnceReturnValue() && this.removeListener(t, r.listener);
                return this
            }, u.trigger = i("emitEvent"), u.emit = function(t) {
                var e = Array.prototype.slice.call(arguments, 1);
                return this.emitEvent(t, e)
            }, u.setOnceReturnValue = function(t) {
                return this._onceReturnValue = t, this
            }, u._getOnceReturnValue = function() {
                return !this.hasOwnProperty("_onceReturnValue") || this._onceReturnValue
            }, u._getEvents = function() {
                return this._events || (this._events = {})
            }, e.noConflict = function() {
                return a.EventEmitter = c, e
            }, void 0 !== (r = function() {
                return e
            }.call(a, n, a, t)) && (t.exports = r)
        }).call(this)
    }, function(t, e) {}, function(t, e, n) {
        "use strict";
        var r = Object.freeze({
            ABORTED: {
                code: 1e3,
                message: "Media playback aborted error"
            },
            NETWORK: {
                code: 2e3,
                message: "Network error"
            },
            DECODE: {
                code: 3e3,
                message: "Media resource decoding error"
            },
            FORMAT_NOT_SUPPORTED: {
                code: 4e3,
                message: "Resource format not supported error"
            },
            NOT_AUTHORIZED: {
                code: 5e3,
                message: "Authorization error"
            },
            RENDERER_NOT_AVAILABLE: {
                code: 6e3,
                message: "Renderer not available"
            }
        });
        e.a = r
    }, function(t, e, n) {
        "use strict";

        function r() {
            var t;
            t = window.self !== window.parent ? document.referrer : window.location.href;
            var e = n.i(o.a)(t),
                r = e.host.split(".").slice(-2).join(".");
            return r === i || r === u
        }
        e.a = r;
        var o = n(51),
            i = "twitch.tv",
            u = "twitch.tech"
    }, , function(t, e, n) {
        "use strict";

        function r(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
        }

        function o(t) {
            return u()(t) ? document.getElementById(t) : t
        }
        Object.defineProperty(e, "__esModule", {
            value: !0
        }), n.d(e, "PlayerEmbed", function() {
            return v
        });
        var i = n(50),
            u = n.n(i),
            a = n(49),
            c = n(155),
            s = n(154),
            f = function() {
                function t(t, e) {
                    for (var n = 0; n < e.length; n++) {
                        var r = e[n];
                        r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(t, r.key, r)
                    }
                }
                return function(e, n, r) {
                    return n && t(e.prototype, n), r && t(e, r), e
                }
            }(),
            l = Object.freeze({
                ABORTED: s.a.ABORTED.code,
                NETWORK: s.a.NETWORK.code,
                DECODE: s.a.DECODE.code,
                FORMAT_NOT_SUPPORTED: s.a.FORMAT_NOT_SUPPORTED.code,
                NOT_AUTHORIZED: s.a.NOT_AUTHORIZED.code,
                RENDERER_NOT_AVAILABLE: s.a.RENDERER_NOT_AVAILABLE.code
            }),
            p = Object.freeze({
                MINUTE_WATCHED: a.a,
                VIDEO_PLAY: a.b,
                BUFFER_EMPTY: a.c,
                VIDEO_ERROR: a.d
            }),
            h = Object.freeze({
                TRANSITION_TO_RECOMMENDED_VOD: a.e
            }),
            v = function() {
                function t(e, n) {
                    r(this, t), this._bridge = new a.f(o(e), n)
                }
                return f(t, [{
                    key: "play",
                    value: function() {
                        this._bridge.callPlayerMethod(a.g)
                    }
                }, {
                    key: "pause",
                    value: function() {
                        this._bridge.callPlayerMethod(a.h)
                    }
                }, {
                    key: "seek",
                    value: function(t) {
                        this._bridge.callPlayerMethod(a.i, t)
                    }
                }, {
                    key: "setVolume",
                    value: function(t) {
                        this._bridge.callPlayerMethod(a.j, t)
                    }
                }, {
                    key: "setTheatre",
                    value: function(t) {
                        this._bridge.callPlayerMethod(a.k, t)
                    }
                }, {
                    key: "setFullscreen",
                    value: function(t) {
                        this._bridge.callPlayerMethod(a.l, t)
                    }
                }, {
                    key: "setMuted",
                    value: function(t) {
                        this._bridge.callPlayerMethod(a.m, t)
                    }
                }, {
                    key: "setChannel",
                    value: function(t) {
                        this._bridge.callPlayerMethod(a.n, t)
                    }
                }, {
                    key: "setCollection",
                    value: function(t) {
                        var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "",
                            n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "";
                        this._bridge.callPlayerMethod(a.o, t, e, n)
                    }
                }, {
                    key: "setVideo",
                    value: function(t) {
                        var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "";
                        this._bridge.callPlayerMethod(a.p, t, e)
                    }
                }, {
                    key: "setContent",
                    value: function(t) {
                        var e = t.contentId,
                            n = t.customerId;
                        this._bridge.callPlayerMethod(a.q, e, n)
                    }
                }, {
                    key: "setClip",
                    value: function(t) {
                        this._bridge.callPlayerMethod(a.r, t)
                    }
                }, {
                    key: "setQuality",
                    value: function(t) {
                        this._bridge.callPlayerMethod(a.s, t)
                    }
                }, {
                    key: "setWidth",
                    value: function(t) {
                        this._bridge.setWidth(t)
                    }
                }, {
                    key: "setHeight",
                    value: function(t) {
                        this._bridge.setHeight(t)
                    }
                }, {
                    key: "setMiniPlayerMode",
                    value: function(t) {
                        this._bridge.callPlayerMethod(a.t, t)
                    }
                }, {
                    key: "setTrackingProperties",
                    value: function(t) {
                        n.i(c.a)() && this._bridge.callPlayerMethod(a.u, t)
                    }
                }, {
                    key: "setPlayerType",
                    value: function(t) {
                        this._bridge.callPlayerMethod(a.v, t)
                    }
                }, {
                    key: "addEventListener",
                    value: function(t, e) {
                        this._bridge.addEventListener(t, e)
                    }
                }, {
                    key: "removeEventListener",
                    value: function(t, e) {
                        this._bridge.removeEventListener(t, e)
                    }
                }, {
                    key: "enableCaptions",
                    value: function() {
                        this._bridge.callPlayerMethod(a.w)
                    }
                }, {
                    key: "disableCaptions",
                    value: function() {
                        this._bridge.callPlayerMethod(a.x)
                    }
                }, {
                    key: "getContentId",
                    value: function() {
                        return this._bridge.getStoreState().stream.contentId
                    }
                }, {
                    key: "getChannel",
                    value: function() {
                        return this._bridge.getPlayerState().channelName
                    }
                }, {
                    key: "getCurrentTime",
                    value: function() {
                        return this._bridge.getPlayerState().currentTime
                    }
                }, {
                    key: "getCustomerId",
                    value: function() {
                        return this._bridge.getStoreState().stream.customerId
                    }
                }, {
                    key: "getDuration",
                    value: function() {
                        return this._bridge.getPlayerState().duration
                    }
                }, {
                    key: "getEnded",
                    value: function() {
                        return this._bridge.getPlayerState().playback === a.y
                    }
                }, {
                    key: "getMuted",
                    value: function() {
                        return this._bridge.getPlayerState().muted
                    }
                }, {
                    key: "getPlaybackStats",
                    value: function() {
                        return this._bridge.getStoreState().stats.videoStats
                    }
                }, {
                    key: "getPlaySessionId",
                    value: function() {
                        return this._bridge.getStoreState().playSessionId
                    }
                }, {
                    key: "isPaused",
                    value: function() {
                        return this._bridge.getPlayerState().playback === a.z
                    }
                }, {
                    key: "getQuality",
                    value: function() {
                        return this._bridge.getPlayerState().quality
                    }
                }, {
                    key: "getQualities",
                    value: function() {
                        return this._bridge.getPlayerState().qualitiesAvailable
                    }
                }, {
                    key: "getViewers",
                    value: function() {
                        return this._bridge.getStoreState().viewercount
                    }
                }, {
                    key: "getVideo",
                    value: function() {
                        return this._bridge.getPlayerState().videoID
                    }
                }, {
                    key: "getVolume",
                    value: function() {
                        return this._bridge.getPlayerState().volume
                    }
                }, {
                    key: "getTheatre",
                    value: function() {
                        return this._bridge.getStoreState().screenMode.isTheatreMode
                    }
                }, {
                    key: "getFullscreen",
                    value: function() {
                        return this._bridge.getStoreState().screenMode.isFullScreen
                    }
                }, {
                    key: "getFullscreenEnabled",
                    value: function() {
                        return this._bridge.getStoreState().screenMode.canFullScreen
                    }
                }, {
                    key: "getSessionInfo",
                    value: function() {
                        return {
                            broadcastId: this._bridge.getStoreState().broadcastId,
                            playSessionId: this._bridge.getStoreState().playSessionId
                        }
                    }
                }, {
                    key: "getCaptionsAvailable",
                    value: function() {
                        return this._bridge.getStoreState().captions.available
                    }
                }, {
                    key: "_addPlayerStateListener",
                    value: function(t) {
                        this._bridge.addPlayerStateListener(t)
                    }
                }, {
                    key: "_removePlayerStateListener",
                    value: function(t) {
                        this._bridge.removePlayerStateListener(t)
                    }
                }, {
                    key: "destroy",
                    value: function() {
                        this._bridge.destroy()
                    }
                }], [{
                    key: "READY",
                    get: function() {
                        return a.A
                    }
                }, {
                    key: "PLAY",
                    get: function() {
                        return a.B
                    }
                }, {
                    key: "PAUSE",
                    get: function() {
                        return a.C
                    }
                }, {
                    key: "ENDED",
                    get: function() {
                        return a.D
                    }
                }, {
                    key: "ONLINE",
                    get: function() {
                        return a.E
                    }
                }, {
                    key: "OFFLINE",
                    get: function() {
                        return a.F
                    }
                }, {
                    key: "ERROR",
                    get: function() {
                        return a.G
                    }
                }]), t
            }();
        window.Twitch = window.Twitch || {}, window.Twitch.Player = v, window.Twitch.Analytics = p, window.Twitch.Errors = l, window.Twitch.EmbedEvents = h
    }])
});