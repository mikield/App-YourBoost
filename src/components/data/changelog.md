# Huray!
<img src='~assets/trump.gif' />

BoostMy.Stream is now officially live. Project is tagged as version 1.0.0
It took a long time to achieve this time line, but the new Update brings something new. Lets start...

* Main page has now slider with a preview of service pages (instead of a static image)
* New Accounts Page.

    This page brings you a option to control your added accounts and resolves several technical issues, for example: now we are storing your access token and your user experience will be better.
    Of course if your account (f.e. youtube) will have access to several channels you could attach them to the system. Thats why we need such solution.
    
* Mixer.com will now be supported (currently only adding a account and attaching the channel)
* New login options: Vk.com, Twitter.com, Mixer.com

    Please be sure to use the same email address on every service, cause this is your first login unique parameter.
    
* English language is now prior #1. Other languages will be supported (some of them could be translated by community)
 
 
 ### And of course the new API
 The new API is <span class='text-sucess'>30%</span> faster, it was fully rewritten and given some pretty boost. 🤗

 The new API is more secure, and actually gets a good REST. 😎
 
 
 # What's next?
 The next step is - develop, create awesome stuff and do your streaming time easier. 😉