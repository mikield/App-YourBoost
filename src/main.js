import Vue from 'vue'
import {App, Store, Services} from 'src/modules'
import VueProgressBar from 'vue-progressbar'


Vue.use(Services.Router.Service)
//Request Use in in the services/Request.js file (to configure the Vue.http)
Vue.use(Services.Filters)
Vue.use(Services.Tooltip)
Vue.use(Services.Meta)
//Translate Use is in the services/Translate.js file (it needs Vue instance)
Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '2px'
  })

// Some application configuration
Vue.config.debug = process.env.NODE_ENV !== 'production'
Vue.config.devtools = process.env.NODE_ENV !== 'production'

new App(Services.Router.Routes, Store, Services.Translate.Instance).run()
