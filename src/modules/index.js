import App from 'src/App'
import Store from 'store'
import * as Services from 'src/modules/services'

module.exports =  {
  App, Store, Services
}
