import ApplicationLayout from 'components/layout/App.vue'
import AuthLayout from 'components/layout/Auth.vue'
import FrontLayout from 'components/layout/Front.vue'

module.exports = {
  ApplicationLayout, AuthLayout, FrontLayout
}
