import Welcome from 'components/Welcome.vue'
import Dashboard from 'components/Dashboard.vue'
import Promotions from 'components/Promotions.vue'
import View from 'components/View.vue'
import Help from 'components/pages/Help.vue'
import FAQ from 'pages/FAQ.vue'
import Challenges from 'components/pages/Challenges.vue'
import Hosting from 'components/Hosts.vue'
import Login from 'components/Login.vue'
import Events from 'components/Events.vue'
import Settings from 'components/Settings.vue'
import Accounts from 'components/pages/Accounts.vue'

const WelcomeRoutes = [{ path: '/', component: Welcome, name: 'welcome' }]

const AuthRoutes = [
  { path: '/login', component: Login, name: 'login', meta: { guest: true } },
  { path: '/faq', component: FAQ, name: 'FAQ' }
]

const AppRoutes = [
  {
    path: '/dashboard',
    component: Dashboard,
    name: 'dashboard',
    meta: { auth: true }
  },
  {
    path: '/promotions',
    component: Promotions,
    name: 'promotions',
    meta: { auth: true }
  },
  {
    path: '/view/:promotionId',
    component: View,
    name: 'view',
    meta: { auth: true }
  },
  { path: '/help', component: Help, name: 'help', meta: { auth: true } },
  // {path: '/challenges', component: Challenges, name: 'challenges', meta: {auth: true} },
  // {path: '/hosts', component: Hosting, name: 'hosts', meta: {auth: true} },
  // {path: '/events', component: Events, name: 'events', meta: {auth: true} },
  // {path: '/settings', component: Settings, name: 'settings', meta: {auth: true}},
  {
    path: '/accounts',
    component: Accounts,
    name: 'accounts',
    meta: { auth: true }
  }
]

module.exports = {
  WelcomeRoutes,
  AuthRoutes,
  AppRoutes
}
