let avaiableModals = {
  'Channel': {
    "Add": require('components/Modals/Channel/Add.vue'),
    "Found": require('components/Modals/Channel/Found.vue')
  },
  "ChangeLog" : require('components/Modals/ChangeLog.vue'),
  "Hosting": {
    "AddToList": require('components/Modals/Hosting/AddToList.vue'),
    "GetPrize": require('components/Modals/Hosting/GetPrize.vue')
  },
  "Events": {
    "New": require('components/Modals/Events/New.vue'),
    "Remove": require('components/Modals/Events/Remove.vue')
  },
  "Stream": {
    "Muted": require('components/Modals/Stream/Muted.vue'),
    "Offline": require('components/Modals/Stream/Offline.vue'),
    "Paused": require('components/Modals/Stream/Paused.vue')
  },
  "Promotion": {
    "Add": require('components/Modals/Promotion/Add.vue')
  }
}

const GetModalBySlashName  = (name) => {
  let split = name.split('/')
  let result = '';
  if(split.length >= 2){
    if(avaiableModals[split[0]] == undefined){
      console.error('Modal namespace not found')
    }else if(avaiableModals[split[0]][split[1]] === undefined){
      console.error('Modal not found')
    }else{
      result = {view: avaiableModals[split[0]][split[1]], name: split[0] + '.' + split[1]}
    }
  }else{
    result = {view: avaiableModals[split[0]], name: split[0]}
  }
  return result;
}

const FindModalByName = (name) => {
  if(state.Opened.has(name)){
    let found = state.Opened.get(name)
    found.name = name
    return found
  }else{
    console.error(`No opened modal with such name ${name}`)
    return
  }
}

// initial state
const state = {
  All: [],
  Opened: new Map
}

// getters
const getters = {

}

// actions
const actions = {
  Display ({ commit }, modal) {
    let data = modal.data || []
    modal = GetModalBySlashName(modal.name)
    commit('Displaied', {modal, data})
  },
  Hide ({commit, dispatch}, modal){
    modal = FindModalByName(modal.name)
    commit('Hidden', modal)
  }
}

// mutations
const mutations = {
  Displaied (state, {modal, data}) {
    if(!document.body.classList.contains('modal-open')){
      document.body.classList.add('modal-open')
    }
    let object = {
      view: modal.view,
      show: true,
      data: data || []
    }
    let length = state.All.push(object)
    object.key = length-1
    state.Opened.set(modal.name, object)
  },
  Hidden (state, modal){
    state.All.splice(modal.key, 1);
    state.Opened.delete(modal.name)
    if(state.Opened.size <= 0){
      if(document.body.classList.contains('modal-open')){
        document.body.classList.remove('modal-open')
      }
    }
  },
  Loading(state, status){
    state.loading = status
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
