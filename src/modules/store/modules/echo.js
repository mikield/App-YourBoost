import toastr from 'toastr'
// initial state
const state = {
  service: null,
  ConnectedToPrivateChannel: true,
  userPrivateChannel: null
}

// getters
const getters = {
  inited: state => (state.service !== undefined)
}

// actions
const actions = {
  Save ({ commit }, EchoService) {
    commit('Saved', EchoService)
  },
  ConnectToPrivateChannel({commit, dispatch, state}, {user, token}){
    if(state.service.connector.channels['user-state.'+user.id] === undefined){
      let channel = state.service.private('user-state.' + user.id)
      channel.on('subscription_error', (statusCode) => {
        commit('ConnectedToPrivateChannel', false)
      })
      commit('SetUserPrivateChannel', channel)
      dispatch('ListenUserPrivateEvents', user)
    }
  },
  ListenUserPrivateEvents({state, dispatch}, user){
    state.userPrivateChannel.on('Notification', (event) => {
      if(event.error !== undefined){
        dispatch('User/Notify', {message: event.error}, {root: true})
      }else{
        dispatch('User/Notify', {message: event.message}, {root: true})
      }
    })

    state.userPrivateChannel.on('Account.Added', (event) => {
      dispatch('User/AddAccount', event.account, {root: true})
      dispatch('User/Notify', {message: "Account was added", status: 'success'}, {root: true})
    })
    state.userPrivateChannel.on('Account.Updated', (event) => {
      dispatch('User/Notify', {message: "Account was updated", status: 'success'}, {root: true})
    })
  }
}

// mutations
const mutations = {
  Saved (state, Echo) {
    state.service = Echo
  },
  UpdateAuth (state, token){
    state.service.connector.options.auth.headers['Authorization'] = localStorage.getItem('access_token');
  },
  ConnectedToPrivateChannel(state, status){
    state.ConnectedToPrivateChannel = status
  },
  SetUserPrivateChannel(state, channel){
    state.userPrivateChannel = channel
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
