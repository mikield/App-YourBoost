import Vue from 'vue'
import {currency, fcupper} from 'src/helpers'
import toastr from 'toastr'

// initial state
const state = {
  info: {
    exists: false
  },
  token: {
    access_token: null,
    refresh_token: null
  }
}

// getters
const getters = {
  isLoggedIn: state => (state.info.exists)
}

// actions
const actions = {
  LogIn ({ commit, dispatch }, event) {
    return new Promise((resolve, reject) => {
      if(event.token && event.user){
        commit('Logged_In', event)
        commit('Echo/UpdateAuth', event.token,  { root: true })
        dispatch('Echo/ConnectToPrivateChannel', {user: event.user, token: event.token}, {root: true})
        resolve({
          loggedIn: true
        })
      }else{
        reject({
          loggedIn: false
        })
      }
    })
  },
  Update ({commit}, user){
    commit('Updated', user)
  },
  LogOut ({ commit }){
    return new Promise((resolve, reject) => {
      commit('Logged_Out')
      commit('Echo/UpdateAuth', null,  { root: true })
      resolve({
        loggedOut: true
      })
    })
  },
  ChooseChannel ({commit}, channel){
    commit('Channel_Choosen', channel)
  },
  AddChannel ({commit}, channel){
    commit('Channel_Added', channel)
  },
  AddAccount ({commit}, account){
    commit('Account_Added', account)
  },
  ForgetChannel ({commit}, channel){
    commit('Channel_Forgotten', channel)
  },
  UpdateBalance ({commit}, balance){
    commit('BalanceUpdated', balance)
  },
  Notify ({commit}, {message, status = null}){
    if(status === null) status = 'info'
    switch (status) {
      case "info":
      toastr.info(message, "Wow")
      break;
      case "success":
      toastr.success(message, "Huray")
      break;
    }
  }
}

// mutations
const mutations = {
  Logged_In (state, {user, token}) {
    var refresh_token, access_token
    if(token.type !== 'auth'){
      refresh_token = token.refreshToken
      access_token = fcupper(token.type)+ " " + token.token
    }else{
      access_token = token.access_token
      refresh_token = state.token.refresh_token
    }
    localStorage.setItem('access_token', `${access_token}`)
    state.info = user
    state.token = {access_token, refresh_token}
    Vue.http.headers.common.Authorization = localStorage.getItem('access_token')
  },
  Logged_Out (state){
    localStorage.removeItem('access_token')
    state.info = {
      exists: false
    }
    state.token = null
    Vue.http.headers.common.Authorization = null
  },
  Updated (stat, user){
    state.info = user
  },
  Channel_Added ({info}, channel){
    if(info.channels === undefined ||info.channels === null){
      info.channels = new Array
    }
    info.channels.push(channel)
  },
  Account_Added ({info}, account){
    if(info.accounts === undefined ||info.accounts === null){
      info.accounts = new Array
    }
    info.accounts.push(account)
  },
  Channel_Choosen (state, channel){
    state.info.channel = channel
  },
  Channel_Forgotten (state, channel){
    state.info.channel = null
  },
  BalanceUpdated (state, balance){
    state.info.balance = balance
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
