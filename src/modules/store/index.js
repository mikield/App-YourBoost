import Vue from 'vue'
import StateManager from 'vuex'
import * as actions from 'store/actions'
import User from 'store/modules/user'
import Echo from 'store/modules/echo'
import Modal from 'store/modules/modal'

Vue.use(StateManager)

export default new StateManager.Store({
  actions,
  modules: {
    User,
    Echo,
    Modal
  }
})
