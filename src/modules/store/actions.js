import Vue from 'vue'

export function CheckAuth({dispatch, state}){
  return new Promise((resolve, reject) => {
    var token = !!localStorage.getItem('access_token')
    if (token) {
      if (!state.User.info.exists) {
        Vue.http.headers.common.Authorization = localStorage.getItem('access_token')
        Vue.http.get('auth').then(({data}) => {
          dispatch('User/LogIn', {
            user: data.message.user,
            token: {
              type: 'auth',
              access_token: localStorage.getItem('access_token')
            }
          })
          resolve(data.message.user)
        }, () => {
          dispatch('User/LogOut')
          resolve({exist: false})
        })
      } else {
        resolve(state.User.info)
      }
    } else {
      resolve({exist: false})
    }
  })
}
