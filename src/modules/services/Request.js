import Vue from 'vue'
import store from 'store'
import Request from 'vue-resource'

Vue.use(Request)

Vue.http.options.root = process.env.API_URL + '/' + process.env.API_VERSION
Vue.http.headers.common.Accept = 'application/json'

Vue.http.interceptors.push((request, next)  => {
  // if(request.method === "PUT" || request.method === 'PATCH'){
  //   if(request.body !== null){
  //     if(request.body instanceof FormData){
  //       request.body.append('_method', request.method)
  //     }else{
  //       request.body['_method'] = request.method
  //     }
  //   }else{
  //     let data = new FormData();
  //     data.append('_method', request.method);
  //     request.body = data
  //   }
  //   request.method = "POST"
  // }
  // continue to next interceptor
  next((response) => {
    if(response.status === 401){
      //Get new token
      store.dispatch('User/LogOut')
    }
  });
});

module.exports = {
  Service: Request
}
