import Router from 'vue-router'
import store from 'store'

import { ApplicationLayout, AuthLayout, FrontLayout } from 'src/modules/layouts'

import { WelcomeRoutes, AuthRoutes, AppRoutes } from 'src/modules/routes'

const routes = new Router({
  mode: 'history',
  routes: [
    { path: '/', component: FrontLayout, children: WelcomeRoutes },
    { path: 'Auth', component: AuthLayout, children: AuthRoutes },
    { path: 'Application', component: ApplicationLayout, children: AppRoutes }
  ]
})

routes.beforeEach((to, from, next) => {
  store.dispatch('CheckAuth').then(user => {
    if (to.meta.auth && !user.exists) {
      next({ name: 'welcome' })
    } else if (to.meta.guest && user.exists) {
      next({ name: 'promotions' })
    } else {
      next()
    }
  })
})

routes.afterEach(() => {
  $('#aside').modal('hide')
  $('body').removeClass('modal-open')
  $('.modal-backdrop').remove()
})

module.exports = {
  Service: Router,
  Routes: routes
}
