import Vue from 'vue'
import Translate from 'vue-i18n'
Vue.use(Translate)

export const langs = {
  ru: require('components/langs/ru'),
  'en-US': require('components/langs/en')
}

const i18n = new Translate({
  locale:
    localStorage.getItem('lang') ||
    navigator.language ||
    navigator.userLanguage,
  messages: langs
})

module.exports = {
  Service: Translate,
  Instance: i18n
}
