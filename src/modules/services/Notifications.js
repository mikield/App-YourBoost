import Notifications from 'toastr'

// Configure Notifications
require('assets/styles/toastr.css')
Notifications.options = {
  positionClass: 'toast-top-center',
  closeButton: true,
  progressBar: true,
  newestOnTop: true,
  preventDuplicates: true
}

module.exports = {
  Service: Notifications
}
