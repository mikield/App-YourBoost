import Echo from 'laravel-echo'
import store from 'store'

store.dispatch('Echo/Save', new Echo({
  broadcaster: 'socket.io',
  host: process.env.API_URL + ':6002'
}))

module.exports = {
  Service: Echo
}
