import Router from 'src/modules/services/Router'
import Request from 'src/modules/services/Request'
import Filters from 'vue2-filters'
import Echo from 'src/modules/services/Echo'
import Notifications from 'src/modules/services/Notifications'
import Tooltip from 'v-tooltip'
import Meta from 'vue-head'
import Translate from 'src/modules/services/Translate'

module.exports = {
  Router,
  Request,
  Filters,
  Echo,
  Notifications,
  Tooltip,
  Meta,
  Translate
}
