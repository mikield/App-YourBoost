let WindowStateManager = {
  _LOCALSTORAGE_KEY: 'WINDOW_VALIDATION',
  RECHECK_WINDOW_DELAY_MS: 100,
  _initialized: false,
  _isMainWindow: false,
  _unloaded: false,
  _windowArray: [],
  _windowId: null,
  _isNewWindowPromotedToMain: false,
  Init (isNewWindowPromotedToMain) {
    this._isNewWindowPromotedToMain = isNewWindowPromotedToMain;
    this._windowId = window.location.href//Date.now().toString();
    this._bindUnload()
    this._determineWindowState()
    this._initialized = true
    return this
  },
  _bindUnload () {
    window.addEventListener('beforeunload', () => {
      if (!this._unloaded)
      {
        this._removeWindow();
      }
    })
    window.addEventListener('unload', () => {
      if (!this._unloaded)
      {
        this._removeWindow();
      }
    })
  },
  _removeWindow (){
    let __windowArray = JSON.parse(localStorage.getItem(this._LOCALSTORAGE_KEY));
    if (__windowArray === null || __windowArray === "NaN" || __windowArray === undefined){
      __windowArray = [];
    }
    for (var i = 0, length = __windowArray.length; i < length; i++){
      if (__windowArray[i] === this._windowId){
        __windowArray.splice(i, 1)
        break
      }
    }
    localStorage.setItem(this._LOCALSTORAGE_KEY, JSON.stringify(__windowArray));
  },
  _determineWindowState() {
    var self = this;
    var _previousState = this._isMainWindow;
    var _matched = false;

    this._windowArray = localStorage.getItem(this._LOCALSTORAGE_KEY);

    let str = window.location.href;
    let re = /view\/[a-z0-9A-Z]*/g
    let found = str.match(re)
    if(found!= null && found.length >= 1){
      var _matched = true;
    }

    if (this._windowArray === null || this._windowArray === "NaN" || this.__windowArray === undefined){
      this._windowArray = [];
    }else{
      this._windowArray = JSON.parse(this._windowArray);
    }

    if (this._initialized){
      //Determine if this window should be promoted

      if (this._windowArray.length <= 1 || (this._isNewWindowPromotedToMain ? this._windowArray[this._windowArray.length - 1] : this._windowArray[0]) === this._windowId){
        this._isMainWindow = true;
      }else{
        this._isMainWindow = false;
      }
    }else{
      if(_matched){
        if (this._windowArray.length === 0){
          this._isMainWindow = true;
          this._windowArray[0] = this._windowId;
          localStorage.setItem(this._LOCALSTORAGE_KEY, JSON.stringify(this._windowArray));
        }else{
          this._isMainWindow = false;
          this._windowArray.push(this._windowId);
          localStorage.setItem(this._LOCALSTORAGE_KEY, JSON.stringify(this._windowArray));
        }
      }
    }
    //Perform a recheck of the window on a delay
    setTimeout(() => {
      this._determineWindowState()
    }, this.RECHECK_WINDOW_DELAY_MS);
  },

  //Public
  isMainWindow(){
    return this._isMainWindow
  },
  resetWindows(){
    localStorage.set(this._LOCALSTORAGE_KEY, []);
  },
  shouldBlock(){
    return this._windowArray.length > 3;
  },
}
module.exports = WindowStateManager.Init(false);
