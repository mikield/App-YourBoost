var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_URL: '"http://localhost"',
  API_VERSION: '"v1"',
  API_V_URL: '"http://localhost/v1"'
})
