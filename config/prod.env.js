module.exports = {
  NODE_ENV: '"production"',
  API_URL: '"https://api.boostmy.stream"',
  API_VERSION: '"v1"',
  API_V_URL: '"https://api.boostmy.stream/v1"'
}
