FROM node:9.3.0

MAINTAINER Vladyslav Gaysyuk <me@mikield.rocks>

COPY package*.json /tmp/
RUN cd /tmp && yarn
RUN mkdir -p /usr/src/app && cp -a /tmp/node_modules /usr/src/app

WORKDIR /usr/src/app

COPY . /usr/src/app

EXPOSE 3333

CMD [ "npm", "run", "dev" ]
